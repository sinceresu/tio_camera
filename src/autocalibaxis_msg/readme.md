## calib_axis_status.msg ##
topic名称“/tio/precalib/calib_status”
向外发送当前状态
task_status=-1：异常
包括，
a.读取点云地图异常
b.上下左右4个预置位外参参数个数不够
c.接收不到雷达数据
task_status=0：正常结束整个自标定
包括，
a.计算旋转轴正常结束
task_status=1：结束一个预置位的计算，可旋转到下一帧进行计算

## calib_one_pose.srv ##
topic名称"/tio/precalib/calib_one_pose_exRT"
当电机旋转到（水平0，向下30），（水平0，向下60），（水平60，向下0），（水平300，向下0）时发送该请求，请求中的roll_angle，pitch_angle分别与之对应：
（水平0，向下30） roll_angle，pitch_angle=0,30
（水平0，向下60） roll_angle，pitch_angle=0,60
（水平60，向下0） roll_angle，pitch_angle=60,0
（水平300，向下0） roll_angle，pitch_angle=300,0
string station_map_version 地图版本号
string map_format 地图格式后缀
地图位置默认在"$(find mapping_service)/data/pcdFiles"
触发旋转轴计算，计算结束后会发出task_status=1

## calib_run.srv ##
topic名称"/tio/precalib/calib_axis"
触发旋转轴计算，计算结束后会发出task_status=0
文件生成在/xx/autocalibaxis_service/config/axis_paramer.yaml