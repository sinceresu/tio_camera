#include "ros/ros.h"
#include "calibaxis.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "auto_calib_axis_node");
    CalibAxis locator;
    ros::Rate loop_rate(10);
    while(ros::ok()){
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}