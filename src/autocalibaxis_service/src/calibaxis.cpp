#include "tool.h"
#include "calibaxis.h"
CalibAxis::CalibAxis() {
    ros::NodeHandle n;
    ROS_INFO("start tf_ircam_pose");
    newpcd_time = ros::Time::now();
    getparamer();
    map.resize(0);
    DataInfo2H.resize(0);
    DataInfo2V.resize(0);
    PLYfilenames.resize(0);
    //ReadCalibrationFile(calibfilepath, ir_lidar_exRT);
    //star_calib_axis = n.advertiseService("/tio/precalib/init_calib_axis",  &CalibAxis::init, this);//载入地图等初始化
    star_calib_one_pose = n.advertiseService("/tio/precalib/calib_one_pose_exRT", &CalibAxis::calib_one_pose,
                                             this);//接受电机一个角度，计算当前外参
    star_calib_run = n.advertiseService("/tio/precalib/calib_axis", &CalibAxis::g2ocalib_axis, this);//收集够五个角度之后，g2o开始计算旋转轴
    sub_livox_msg = n.subscribe("/livox/lidar", 1, &CalibAxis::sublivoxCbk, this);
    pub_pose_status = n.advertise<autocalibaxis_msg::calib_axis_status>("/tio/precalib/calib_status", 1);
    std::thread
    calibthd(std::bind(&CalibAxis::calibpose, this));
    calibthd.detach();
    std::thread
    checkthd(std::bind(&CalibAxis::CheckTopicTimeOut, this));
    checkthd.detach();
}
bool CalibAxis::calib_one_pose(autocalibaxis_msg::star_calib_one_pose::Request &req,autocalibaxis_msg::star_calib_one_pose::Response &res)
{//开始采集数据，读取当前角度，采集足够帧数后，计算定位
    ROS_INFO("calib_one_pose recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl
            <<"V_angle is "<<req.pitch_angle<<std::endl
            <<"H_angle is "<<req.roll_angle<<std::endl
            <<"stationMapVersion is: "<<req.station_map_version<<std::endl
            <<"mapFormat is: "<<req.map_format<<std::endl;
    if(map.size()==0||Map_id!=req.map_id||strcmp(plyname.c_str(),(req.station_map_version).c_str())!=0||strcmp(mapFormat.c_str(),(req.map_format).c_str())!=0)
        //first localize,no map date or new map task
    {
        Device_id=req.device_id;
        Task_id=req.task_id;
        Map_id=req.map_id;
        plyname=req.station_map_version;
        mapFormat=req.map_format;
        std::string mappath=datapath+"/"+int2str(Map_id)+"/ply/"+plyname+"."+mapFormat;
        std::cout<<"need to load map: "<<mappath<<std::endl;
        if (access(mappath.c_str(), 0) == -1)
        {
            printlr("ERROR: The map is not exist!\n");
            std::cout<<"The filepath "<<mappath<<" is not exist."<<std::endl;
            autocalibaxis_msg::calib_axis_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="The map is not exist.";
            pub_pose_status.publish(web_task_msg);
            res.success =false;
            res.message="The map is not exist.";
            return false;
        }
        if(strcmp("ply",(req.map_format).c_str())==0) {
            if (pcl::io::loadPLYFile(mappath, inputmap) == -1) {
                PCL_ERROR("Couldn't read map files\n");
                autocalibaxis_msg::calib_axis_status web_task_msg;
                web_task_msg.device_id = Device_id;
                web_task_msg.task_id = Task_id;
                web_task_msg.map_id = Map_id;
                web_task_msg.task_status = -1;//异常
                web_task_msg.error_message = "Couldn't read map files.";
                pub_pose_status.publish(web_task_msg);
                res.success = false;
                res.message = "Couldn't read map files.";
                return false;
            }
        }
        else if(strcmp("pcd",(req.map_format).c_str())==0)
        {
            if (pcl::io::loadPCDFile(mappath, inputmap) == -1) {
                PCL_ERROR("Couldn't read map files\n");
                autocalibaxis_msg::calib_axis_status web_task_msg;
                web_task_msg.device_id = Device_id;
                web_task_msg.task_id = Task_id;
                web_task_msg.map_id = Map_id;
                web_task_msg.task_status = -1;//异常
                web_task_msg.error_message = "Couldn't read map files.";
                pub_pose_status.publish(web_task_msg);
                res.success = false;
                res.message = "Couldn't read map files.";
                return false;
            }
        }
        UniformSamplingfilter(inputmap,map);
        inputmap.resize(0);
    }
    H_angle=req.roll_angle;
    V_angle=req.pitch_angle;
    IsLocated=false;
    IsPubable=true;//唯一
    nframe=0;
    frame.resize(0);
    res.success =true;
    res.message="start_calib_one_pose";
//    //offline test
//    std::cout<<"testing...."<<std::endl;
//    sleep(1);
//    nframe=MaxFrames;
//    //offline test
    return true;
}
bool CalibAxis::g2ocalib_axis(autocalibaxis_msg::calib_run::Request &req,autocalibaxis_msg::calib_run::Response &res)
{//g2o计算旋转轴
    ROS_INFO("g2ocalib_axis recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl;
    if(DataInfo2H.size()!=2||DataInfo2V.size()!=2){
        printlr("ERROR: Extrinsic Matrixs are not enough!\n");
        std::cout<<"Extrinsic Matrixs are not enough "<<int2str(DataInfo2H.size())<<int2str(DataInfo2V.size())<<std::endl;
        autocalibaxis_msg::calib_axis_status web_task_msg;
        web_task_msg.device_id=Device_id;
        web_task_msg.task_id=Task_id;
        web_task_msg.map_id=Map_id;
        web_task_msg.task_status=-1;//异常
        web_task_msg.error_message="Extrinsic Matrixs are not enough "+int2str(DataInfo2H.size())+int2str(DataInfo2V.size());
        pub_pose_status.publish(web_task_msg);
        res.success =false;
        res.message="Extrinsic Matrixs are not enough.";
        return false;
    }
    std::vector<std::string> yamlfilepath;
    yamlfilepath.resize(0);
    //保存0轴yaml文件到本地
    yamlfilepath.push_back(SaveCalibrationFile0(temppcdpath,DataInfo2H[0].H,DataInfo2H[0].frame_map_exRT,DataInfo2H[1].H,DataInfo2H[1].frame_map_exRT));
    //保存1轴yaml文件到本地
    yamlfilepath.push_back(SaveCalibrationFile1(temppcdpath,DataInfo2V[0].V,DataInfo2V[0].frame_map_exRT,DataInfo2V[1].V,DataInfo2V[1].frame_map_exRT));
    std::vector<Eigen::Isometry3d> se3s;
    std::vector<double> radian;
    std::vector<Eigen::Isometry3d> refs;
    std::vector<double> refrads;
    Eigen::Vector3d trans_world;
    int num =2;
    std::string fileto=calibfilepath+"/axis_paramer.yaml";
    cv::FileStorage fwrite(fileto, cv::FileStorage::WRITE);
    fwrite << "num" << num;
    for (int i = 0; i < num; ++i) {
        Eigen::Isometry3d ref = Eigen::Isometry3d::Identity();
        bool readres = ReadFile(yamlfilepath[i], se3s, radian, i, refrads, trans_world);
        if (!readres) {
            std::cout << "Read file " << yamlfilepath[i] << " failed!" << std::endl;
            res.success = false;
            res.message = "Read file " + yamlfilepath[i] + " failed!";
            return false;
        }
        ref = GetRefSE3(refs, refrads);
        Eigen::Isometry3d t11 = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d t22 = Eigen::Isometry3d::Identity();
        double error = 1e10;

        Eigen::Isometry3d t1_temp = Eigen::Isometry3d::Identity();
        Eigen::Isometry3d t2_temp = Eigen::Isometry3d::Identity();
        double error_temp;
        for (int j = 0; j < 4; ++j)
        {
            for (int k = 0; k < 4; ++k)
            {
                for (int l = 0; l < 4; ++l)
                {
                    t1_temp = Eigen::Isometry3d::Identity();
                    t1_temp.linear() = (Eigen::AngleAxisd(1.5707963267948966 * j, Eigen::Vector3d::UnitZ()) *
                                        Eigen::AngleAxisd(0.7853981633974483 * k, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(0.7853981633974483 * l, Eigen::Vector3d::UnitX()))
                            .toRotationMatrix();
                    for (int m = 0; m < 4; ++m)
                    {
                        for (int n = 0; n < 4; ++n)
                        {
                            for (int o = 0; o < 4; ++o)
                            {
                                t2_temp = Eigen::Isometry3d::Identity();
                                t2_temp.linear() = (Eigen::AngleAxisd(0.7853981633974483 * m, Eigen::Vector3d::UnitZ()) *
                                                    Eigen::AngleAxisd(0.7853981633974483 * n, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(0.7853981633974483 * o, Eigen::Vector3d::UnitX()))
                                        .toRotationMatrix();

                                GetSE3ByPoints(ref, se3s, radian, t1_temp, t2_temp, error_temp);
                                if (error_temp < error)
                                {
                                    t11 = t1_temp;
                                    t22 = t2_temp;
                                    error = error_temp;
                                }
                            }
                        }
                    }
                }
            }
        }

        t22.translation()[1] = 0;
        t22.translation()[2] = 0;
        cv::Mat cvmat;
        cv::eigen2cv(t11.matrix(), cvmat);
        std::stringstream ss;
        ss << "Error_" << i;
        fwrite << ss.str() << error;
        ss.str("");
        ss << "Matrix_" << i;
        if (i == 0)
        {
            std::cout << trans_world << std::endl;
            cvmat.at<double>(0, 3) += trans_world(0);
            cvmat.at<double>(1, 3) += trans_world(1);
            cvmat.at<double>(2, 3) += trans_world(2);
        }
        fwrite << ss.str() << cvmat;
        std::cout << "Rotation " << i << ":" << std::endl
                  << t11.rotation()
                  << std::endl
                  << "Translation " << i << ":" << std::endl
                  << t11.translation().transpose()
                  << std::endl
                  << "Error " << i << ": " << error << std::endl;

        if (i == num - 1)
        {
            cv::Mat cvtool;
            cv::eigen2cv(t22.matrix(), cvtool);
            fwrite << "Tool" << cvtool;
            std::cout << "Tool Rotation:" << std::endl
                      << t22.linear()
                      << std::endl
                      << "Tool Translation:" << std::endl
                      << t22.translation().transpose() << std::endl;
        }

        refs.push_back(t11);
    }
    autocalibaxis_msg::calib_axis_status web_task_msg;
    web_task_msg.device_id=Device_id;
    web_task_msg.task_id=Task_id;
    web_task_msg.map_id=Map_id;
    web_task_msg.task_status=0;//正常结束
    web_task_msg.error_message="g2ocalib_axis finished";
    pub_pose_status.publish(web_task_msg);
    res.success =true;
    res.message="g2ocalib_axis finished";
    map.resize(0);
    DataInfo2H.resize(0);
    DataInfo2V.resize(0);
    return true;
}
void CalibAxis::sublivoxCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in)
{
    newpcd_time = ros::Time::now();
    pcl::PointCloud<pcl::PointXYZ> laserCloudIn;
    pcl::fromROSMsg(*livox_msg_in, laserCloudIn);
    //可发送tf，但帧数不够01
    if(nframe<MaxFrames&&IsPubable)
    {
        frame.insert(frame.end(), laserCloudIn.begin(), laserCloudIn.end());
        nframe++;
    }
}
void CalibAxis::calibpose()
{
    while (1) {
        sleep(0.1);

        if(!IsLocated&&IsPubable&&nframe==MaxFrames)
        {
            IsLocated = true;//唯一
            double dur;
            clock_t start, end;
            start = clock();
            std::cout<<"+++calibing pose ( "<<H_angle<<","<<V_angle<<" ) +++"<<std::endl;
            Eigen::Matrix4d transform = initMatrix(H_angle,V_angle);
            cv::eigen2cv(transform, init_pos_exRT);
//            //offline test
//            std::string famepcdpath="/home/wxy/workspace/data/1124/autocalibtest/"+float2str(H_angle)+"_"+float2str(V_angle)+".pcd";
//            if(pcl::io::loadPCDFile(famepcdpath, frame) == -1) {
//                std::cout << "load pcd file: " << famepcdpath << "failed." << std::endl;
//                exit(1);
//            }
//
//            //offline test
            pcl::PointCloud<pcl::PointXYZ>::Ptr init_cloud(new pcl::PointCloud<pcl::PointXYZ>());
            pcl::transformPointCloud(frame, *init_cloud, transform); //粗配位置
            initpcdpath=temppcdpath+"/"+float2str(H_angle)+"_"+float2str(V_angle)+"init.pcd";
            pcl::io::savePCDFile(initpcdpath, *init_cloud);

            try {
                getneighbors(map, *init_cloud, neiborinmap);
                std::cout<<"getneighbors finished"<<std::endl;
                transform = ndtprocess(*init_cloud, neiborinmap);//ndt细匹配
                neiborinmap.resize(0);
                std::cout<<"ndtprocess finished"<<std::endl;
                cv::eigen2cv(transform, ndt_pose_exRT);
                //ir_lidar_exRT.convertTo(ir_lidar_exRT, CV_64F);
                init_pos_exRT.convertTo(init_pos_exRT, CV_64F);
                ndt_pose_exRT.convertTo(ndt_pose_exRT, CV_64F);
                //Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();
                //cv::cv2eigen(ir_lidar_exRT, RT1_trans.matrix());
                cv::cv2eigen(init_pos_exRT, RT2_trans.matrix());
                cv::cv2eigen(ndt_pose_exRT, RT3_trans.matrix());
                Eigen::Isometry3d delta = RT3_trans * RT2_trans;
                cv::eigen2cv(delta.matrix(), current_map_exRT);
                std::cout << "current frame->map exRT:" << std::endl
                          << current_map_exRT << std::endl;
                //存入到结构体中
                DataInfo posedata;
                posedata.H=H_angle;
                posedata.V=V_angle;
                posedata.frame_map_exRT=current_map_exRT.clone();
                if(H_angle!=0.0f&&V_angle==0.0f) {
                    DataInfo2H.push_back(posedata);
                }
                else if(V_angle!=0.0f&&H_angle==0.0f)
                {
                    DataInfo2V.push_back(posedata);
                }
                cv::cv2eigen(current_map_exRT, transform);
                pcl::transformPointCloud(frame, *init_cloud, transform); //粗配+细配
                initpcdpath=temppcdpath+"/"+float2str(H_angle)+"_"+float2str(V_angle)+"ndt.pcd";
                pcl::io::savePCDFile(initpcdpath, *init_cloud);
                frame.resize(0);
                Eigen::Matrix3d rotation_matrix;
                cv::Mat handin_R = current_map_exRT(cv::Rect(0, 0, 3, 3));
                cv::cv2eigen(handin_R, rotation_matrix);
                Eigen::Vector3d eulerAngle = rotation_matrix.eulerAngles(2, 1, 0);
                std::cout << "angle (z,y,x): " << std::endl
                          << "(" << eulerAngle(0) * rad << " , " << eulerAngle(1) * rad << " , " << eulerAngle(2) * rad << ")" << std::endl;
                end = clock();
                dur = (double) (end - start);
                printf("using Time:%f ms\n ", (dur / CLOCKS_PER_SEC));
                IsCalibFinished=true;   //唯一
            }
            catch(std::exception ex)
            {
                printlr("Error: localize faile, recollect current sector and retrying......\n");
//                PCL_ERROR("Build map failed in %s \n",filenumstr);
//                std_msgs::Int32 msg;
//                msg.data = 0;
                nframe = 0;
//                pub_nframes.publish(msg);
            }
        }
        else if(IsCalibFinished&&IsLocated&&IsPubable)
        {
            autocalibaxis_msg::calib_axis_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=1;//当前角度计算完成，可以发送下一帧
            web_task_msg.error_message=float2str(H_angle)+"_"+float2str(V_angle);
            pub_pose_status.publish(web_task_msg);
            IsLocated=false;
            IsPubable=false;
            IsCalibFinished=false;
            nframe=0;
            frame.resize(0);
        }

    }

}
void CalibAxis::getparamer()
{

    cout << "Get the parameters from the launch file" << endl;
    if (!ros::param::get("calibfilepath", calibfilepath))
    {
        cout << "Can not get the value of calibfilepath" << endl;
        exit(1);
    }
    if (!ros::param::get("motorposetopic", motorposetopic))
    {
        cout << "Can not get the value of motorposetopic" << endl;
        exit(1);
    }
    if (!ros::param::get("MaxFrames", MaxFrames))
    {
        cout << "Can not get the value of MaxFrames" << endl;
        exit(1);
    }
    if (!ros::param::get("lidartopic", lidartopic))
    {
        cout << "Can not get the value of lidartopic" << endl;
        exit(1);
    }
    if (!ros::param::get("mappath", datapath))
    {
        cout << "Can not get the value of mappath" << endl;
        exit(1);
    }
    if (!ros::param::get("temppcdpath", temppcdpath))
    {
        cout << "Can not get the value of temppcdpath" << endl;
        exit(1);
    }

//    if (pcl::io::loadPCDFile<pcl::PointXYZ>(mappath, map) == -1)
//    {
//        PCL_ERROR("Couldn't read mapfile\n");
//        exit(1);
//    }

}
void CalibAxis::CheckTopicTimeOut()
{
    while (1) {
        sleep(0.1);
        if (IsPubable&&(ros::Time::now()-newpcd_time>ros::Duration(60.0f)||newpcd_time-ros::Time::now()>ros::Duration(60.0f)))
        {

            printlr("ERROR: Unable to get pcd from livox lidar!\n");
            autocalibaxis_msg::calib_axis_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="Unable to get pcd from livox lidar.";
            pub_pose_status.publish(web_task_msg);
        }

    }
}