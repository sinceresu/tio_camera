#ifndef TOOL_H
#define TOOL_H
#include "ros/ros.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/PoseStamped.h"
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include "omp.h"
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
//opencv
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
#include <tf/transform_broadcaster.h>
//ros
#include <sensor_msgs/PointCloud2.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"

//pcl
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h> //NDT配准类对应头文件
#include "pclomp/ndt_omp.h"
#include <pcl/octree/octree_search.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/keypoints/uniform_sampling.h>//均匀采样
//g2o
#include <g2o/core/base_vertex.h>
#include <g2o/core/base_binary_edge.h>
#include <g2o/types/slam3d/vertex_se3.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/dense/linear_solver_dense.h>

#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)

using namespace cv;
using namespace std;
using namespace ros;
using namespace pcl;
using namespace sensor_msgs;

typedef Eigen::Matrix<double, 3, 3> Mat33;
typedef Eigen::Matrix<double, 3, 1> Vec3;

void ReadCalibrationFile(std::string _choose_file,cv::Mat &ir_lidar_RT);
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d);
Eigen::Matrix4d ndtprocess(pcl::PointCloud<pcl::PointXYZ>& frame, pcl::PointCloud<pcl::PointXYZ>& map);
void getneighbors(pcl::PointCloud<pcl::PointXYZ> &source, pcl::PointCloud<pcl::PointXYZ> &searchPoint, pcl::PointCloud<pcl::PointXYZ> &neibors);
Eigen::Matrix4d initMatrix(double Rot_H_angel, double Rot_V_angel);
std::string SaveCalibrationFile0(std::string _temppcdpath
                          ,float angle1,cv::Mat m1
                          ,float angle2,cv::Mat m2)
{
    std::string path_filename_str;
    path_filename_str = _temppcdpath+"/extrinsics_0.yaml";

    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    cv::Mat m0=cv::Mat::eye(4, 4, CV_64FC1);
    m1.convertTo(m1, CV_64FC1);
    m2.convertTo(m2, CV_64FC1);
    fs << "num" << 3;
    fs << "radian_0_0" << 0.0f;
    fs << "radian_0_1" << angle1/180.0f*M_PI;
    fs << "radian_0_2" << angle2/180.0f*M_PI;
    fs << "CameraExtrinsicMat_0_0" <<m0 ;
    fs << "CameraExtrinsicMat_0_1" <<m1 ;
    fs << "CameraExtrinsicMat_0_2" <<m2 ;
    std::cout<<"wroten yaml file in: "<<path_filename_str<<std::endl;
    return path_filename_str;
}
std::string SaveCalibrationFile1(std::string _temppcdpath
        ,float angle1,cv::Mat m1
        ,float angle2,cv::Mat m2)
{
    std::string path_filename_str;
    path_filename_str = _temppcdpath+"/extrinsics_1.yaml";

    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }
    cv::Mat m0=cv::Mat::eye(4, 4, CV_64FC1);
    m1.convertTo(m1, CV_64FC1);
    m2.convertTo(m2, CV_64FC1);
    fs << "radian_0" << 0.0f;
    fs << "num" << 3;
    fs << "radian_1_0" << 0.0f;
    fs << "radian_1_1" << angle1/180.0f*M_PI;
    fs << "radian_1_2" << angle2/180.0f*M_PI;
    fs << "CameraExtrinsicMat_1_0" <<m0 ;
    fs << "CameraExtrinsicMat_1_1" <<m1 ;
    fs << "CameraExtrinsicMat_1_2" <<m2 ;
    std::cout<<"wroten yaml file in: "<<path_filename_str<<std::endl;
    return path_filename_str;
}
void UniformSamplingfilter(pcl::PointCloud<pcl::PointXYZ> &in_pc,pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    pcl::UniformSampling<pcl::PointXYZ> filter;
    filter.setInputCloud(cloud);
    filter.setRadiusSearch(0.01f);
    pcl::PointCloud<int> keypointIndices;
    filter.filter(out_pc);

}
//------------
Eigen::Matrix4d ndtprocess(pcl::PointCloud<pcl::PointXYZ>& frame, pcl::PointCloud<pcl::PointXYZ>& map)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr Inputpt(new pcl::PointCloud<pcl::PointXYZ>(frame));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Targetptr(new pcl::PointCloud<pcl::PointXYZ>(map));
    pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
    ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
    ndt->setNumThreads(2);
    ndt->setTransformationEpsilon(0.0001);
    ndt->setStepSize(0.5);
    ndt->setResolution(0.5);
    ndt->setMaximumIterations(10);
    ndt->setInputSource(Inputpt);
    ndt->setInputTarget(Targetptr);
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();
    ndt->align(*output_cloud, init_guess);
    Eigen::Matrix4f transformation = ndt->getFinalTransformation();
    // pcl::transformPointCloud(*Inputpt, *output_cloud, transformation);
    //pcl::io::savePCDFile("../ndticppc.pcd", *output_cloud);
    Eigen::Matrix4d trans_d;
    M4ftoM4d(transformation, trans_d);
    return trans_d;
}
Eigen::Matrix4d initMatrix(double Rot_H_angel, double Rot_V_angel)
{
    cv::Mat init_exRT = cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat init_R = cv::Mat::zeros(3, 3, CV_64F);
    Eigen::Matrix3d rotation_matrix;
    double rad = 180.0f / M_PI;
    Eigen::Matrix4d transform;
    Eigen::AngleAxisd yawAngle, pitchAngle;
    yawAngle = Eigen::AngleAxisd(Rot_H_angel / rad, Eigen::Vector3d::UnitZ());
    pitchAngle = Eigen::AngleAxisd(Rot_V_angel / rad, Eigen::Vector3d::UnitY());
    rotation_matrix = yawAngle * pitchAngle;
    cv::eigen2cv(rotation_matrix, init_R);
    init_R.copyTo(init_exRT(cv::Rect_<double>(0, 0, 3, 3)));
    init_exRT.at<double>(0, 3) = 0.0;
    init_exRT.at<double>(1, 3) = 0.0;
    init_exRT.at<double>(2, 3) = 0.0;
    init_exRT.at<double>(3, 3) = 1.0;
    cv::cv2eigen(init_exRT, transform);
    return transform;
}
void getneighbors(pcl::PointCloud<pcl::PointXYZ> &source, pcl::PointCloud<pcl::PointXYZ> &searchPoint, pcl::PointCloud<pcl::PointXYZ> &neibors)
{
    float resolution = 0.01f;
    pcl::PointCloud<pcl::PointXYZ>::Ptr Sourceptr(new pcl::PointCloud<pcl::PointXYZ>(source));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Searchptr(new pcl::PointCloud<pcl::PointXYZ>(searchPoint));
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
    octree.setInputCloud(Sourceptr);
    octree.addPointsFromInputCloud();

    // K nearest neighbor search

    // int K = 10;
    float radius = 0.1f;
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    for (pcl::PointCloud<pcl::PointXYZ>::iterator pt = searchPoint.begin();
         pt < searchPoint.end(); pt++)
    {
        if (octree.radiusSearch(*pt, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
        {
            for (std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
                neibors.push_back(source[pointIdxRadiusSearch[i]]);
        }
    }
}

void ReadCalibrationFile(std::string _choose_file,cv::Mat &ir_lidar_RT)
{
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraExtrinsicMat"] >> ir_lidar_RT;
    }
}
std::string int2str(int num)
{
    ostringstream oss;
    if (oss << num)
    {
        string str(oss.str());
        return str;
    }
    else
    {
        cout << "[float2str] - Code error" << endl;
        exit(0);
    }
}
std::string float2str(float num){
    ostringstream oss;
    if (oss << num) {
        string str(oss.str());
        return str;
    }
    else {
        cout << "[float2str] - Code error" << endl;
        exit(0);
    }
}
void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
};
void readFileList(std::string path, std::vector<std::string> &files, std::string ext)
{
    struct dirent *ptr;
    DIR *dir;
    dir = opendir(path.c_str());
    while ((ptr = readdir(dir)) != NULL)
    {
        if (ptr->d_name[0] == '.')
            continue;
        std::vector<std::string> strVec;
        SplitString(ptr->d_name, strVec, ".");

        if (strVec.size() >= 2)
        {
            std::string p;
            if (strVec[strVec.size() - 1].compare(ext) == 0)
                files.push_back(p.assign(path).append("/").append(ptr->d_name));
        }
        else
        {
            std::string p;
            readFileList(p.assign(path).append("/").append(ptr->d_name), files, ext);
        }
    }
    closedir(dir);
};
int YDGetPLYDataFilePath(std::string strFileDirectory,
                         std::vector<std::string> &pcdfiles)
{
    readFileList(strFileDirectory, pcdfiles, "ply");
    return pcdfiles.size();
};
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d)
{
    trans_d(0, 0) = transformation(0, 0);
    trans_d(0, 1) = transformation(0, 1);
    trans_d(0, 2) = transformation(0, 2);
    trans_d(0, 3) = transformation(0, 3);
    trans_d(1, 0) = transformation(1, 0);
    trans_d(1, 1) = transformation(1, 1);
    trans_d(1, 2) = transformation(1, 2);
    trans_d(1, 3) = transformation(1, 3);
    trans_d(2, 0) = transformation(2, 0);
    trans_d(2, 1) = transformation(2, 1);
    trans_d(2, 2) = transformation(2, 2);
    trans_d(2, 3) = transformation(2, 3);
    trans_d(3, 0) = transformation(3, 0);
    trans_d(3, 1) = transformation(3, 1);
    trans_d(3, 2) = transformation(3, 2);
    trans_d(3, 3) = transformation(3, 3);
}
class EdgeRobot : public g2o::BaseBinaryEdge<6, Eigen::Isometry3d, g2o::VertexSE3, g2o::VertexSE3>
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    EdgeRobot(const double &angle, const Eigen::Isometry3d &ref) : BaseBinaryEdge<6, Eigen::Isometry3d, g2o::VertexSE3, g2o::VertexSE3>()
    {
        _angle = angle;
        _ref = ref;
    }

    void computeError()
    {
        Eigen::Isometry3d t1 = (static_cast<const g2o::VertexSE3 *>(_vertices[0]))->estimate();
        Eigen::Isometry3d t2 = Eigen::Isometry3d::Identity();
        t2.linear() = Eigen::AngleAxisd(_angle, Eigen::Vector3d::UnitZ()).toRotationMatrix();
        Eigen::Isometry3d t3 = (static_cast<const g2o::VertexSE3 *>(_vertices[1]))->estimate();
        t3.translation()[1] = 0;
        t3.translation()[2] = 0;
        Eigen::Isometry3d delta = t1 * t2 * t3 * (_measurement.inverse() * _ref);
        _error = g2o::internal::toVectorMQT(delta);
        _error *= std::exp(std::max(-1.0 * t3.translation()[0], 0.0));
    }

    void setMeasurement(const Eigen::Isometry3d &m)
    {
        _measurement = m;
    }

    virtual bool read(std::istream &is) {}
    virtual bool write(std::ostream &os) const {}

private:
    double _angle;
    Eigen::Isometry3d _ref;
};
bool ReadFile(std::string config_file, std::vector<Eigen::Isometry3d> &se3s, std::vector<double> &radian, int axis_index, std::vector<double> &refrads, Eigen::Vector3d &trans_world)
{
    se3s.clear();
    radian.clear();
    refrads.clear();

    cv::FileStorage fsSettings(config_file, cv::FileStorage::READ);
    if (!fsSettings.isOpened())
    {
        std::cerr << "readParameters ERROR: Wrong setting path for axis " << axis_index << std::endl;
        return false;
    }
    int num = fsSettings["num"];
    if (num < 3)
    {
        std::cerr << "readParameters ERROR: Observation number for axis " << axis_index << " should not be less than 3" << std::endl;
    }
    for (int i = 0; i < num; ++i)
    {
        cv::Mat cv_RT;
        std::string index("CameraExtrinsicMat_");
        index = index + std::to_string(axis_index) + "_" + std::to_string(i);
        fsSettings[index] >> cv_RT;
        Eigen::Matrix4d eigen_RT;
        cv::cv2eigen(cv_RT, eigen_RT);
        //wxyadd
        if (axis_index == 0 && i == 0)
        {
            trans_world << eigen_RT(0, 3), eigen_RT(1, 3), eigen_RT(2, 3);
            std::cout << "trans_world:" << trans_world << std::endl;
        }


        Eigen::Matrix3d so3_0(eigen_RT.block<3, 3>(0, 0));
        Eigen::Vector3d trans_0(eigen_RT(0, 3) - trans_world(0), eigen_RT(1, 3) - trans_world(1), eigen_RT(2, 3) - trans_world(2));
        Eigen::Isometry3d se3 = Eigen::Isometry3d::Identity();
        se3.linear() = so3_0;
        se3.translation() = trans_0;
        se3s.push_back(se3);

        std::string ind("radian_");
        ind = ind + std::to_string(axis_index) + "_" + std::to_string(i);
        double rad;
        fsSettings[ind] >> rad;
        radian.push_back(rad);
    }
    if (axis_index > 0)
    {
        for (int i = 0; i < axis_index; ++i)
        {
            std::string ind("radian_");
            ind = ind + std::to_string(i);
            double rad;
            fsSettings[ind] >> rad;
            refrads.push_back(rad);
        }
    }
    return true;
}
Eigen::Isometry3d GetRefSE3(const std::vector<Eigen::Isometry3d> &frameseries, const std::vector<double> &distanceseries)
{
    assert(frameseries.size() == distanceseries.size());
    int num = frameseries.size();
    Eigen::Isometry3d refinseries = Eigen::Isometry3d::Identity();
    Eigen::Isometry3d motionframes;
    for (int i = 0; i < num; i++)
    {
        motionframes = Eigen::Isometry3d::Identity();
        motionframes.linear() = Eigen::AngleAxisd(distanceseries[i], Eigen::Vector3d::UnitZ()).toRotationMatrix();
        motionframes.translation() = Eigen::Vector3d(0, 0, 0);
        refinseries = refinseries * frameseries[i] * motionframes;
    }
    return refinseries;
}
bool GetSE3ByPoints(const Eigen::Isometry3d &ref, const std::vector<Eigen::Isometry3d> &se3s, const std::vector<double> &radian, Eigen::Isometry3d &t1, Eigen::Isometry3d &t2, double &error)
{
    int num_p = se3s.size();
    assert(num_p >= 3 && num_p == int(radian.size()));
    g2o::SparseOptimizer optimizer;
    g2o::OptimizationAlgorithmLevenberg *solver = new g2o::OptimizationAlgorithmLevenberg(g2o::make_unique<g2o::BlockSolverX>(g2o::make_unique<g2o::LinearSolverDense<g2o::BlockSolverX::PoseMatrixType>>()));
    optimizer.setAlgorithm(solver);

    g2o::VertexSE3 *v1 = new g2o::VertexSE3();
    g2o::VertexSE3 *v2 = new g2o::VertexSE3();
    v1->setEstimate(t1);
    v1->setId(0);
    optimizer.addVertex(v1);
    v2->setEstimate(t2);
    v2->setId(1);
    optimizer.addVertex(v2);

    for (int i = 0; i < se3s.size(); i++)
    {
        EdgeRobot *edge = new EdgeRobot(radian[i], ref);
        edge->setId(i);
        edge->setVertex(0, v1);
        edge->setVertex(1, v2);
        edge->setMeasurement(se3s[i]);
        Eigen::Matrix<double, 6, 6> temp = Eigen::Matrix<double, 6, 6>::Identity();
        //012是平移权重
        // temp(0, 0) = 0.01;
        // temp(1, 1) = 0.01;
        // temp(2, 2) = 0.01;
        edge->setInformation(temp);
        optimizer.addEdge(edge);
    }
    optimizer.initializeOptimization();
    optimizer.optimize(200);

    t1 = v1->estimate();
    t2 = v2->estimate();
    error = optimizer.chi2();
    error = std::sqrt(error);
    // std::cout << v1->estimate().matrix() << std::endl;
    // std::cout << v2->estimate().matrix() << std::endl;
    // std::cout << optimizer.chi2() << std::endl;
    return true;
}

#endif