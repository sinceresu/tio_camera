#ifndef LOCALIZE_H
#define LOCALIZE_H
#include "geometry_msgs/PoseStamped.h"
#include <sensor_msgs/PointCloud2.h>
#include "autocalibaxis_msg/star_calib_one_pose.h"
#include "autocalibaxis_msg/calib_run.h"
#include "autocalibaxis_msg/calib_axis_status.h"
#include <thread>
//synchronism
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
//opencv
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
//pcl
#include <pcl/point_types.h>
#include <pcl/io/io.h>
//lidar
#include "livox_ros_driver/CustomMsg.h"
//g2o
#include <g2o/core/base_vertex.h>
#include <g2o/core/base_binary_edge.h>
#include <g2o/types/slam3d/vertex_se3.h>
#include <g2o/core/block_solver.h>
#include <g2o/core/optimization_algorithm_levenberg.h>
#include <g2o/solvers/dense/linear_solver_dense.h>
using namespace cv;
using namespace std;
using namespace pcl;
using namespace message_filters;

class CalibAxis
{
public:
    CalibAxis();
private:
    void getparamer();
    void sublivoxCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in);
    bool calib_one_pose(autocalibaxis_msg::star_calib_one_pose::Request &req,autocalibaxis_msg::star_calib_one_pose::Response &res);
    bool g2ocalib_axis(autocalibaxis_msg::calib_run::Request &req,autocalibaxis_msg::calib_run::Response &res);
    void CheckTopicTimeOut();
    void calibpose();
private:
    double rad=180.0f/M_PI;
    bool IsLocated=false;//表示是否定位计算完成
    bool IsPubable=false;//表示是否广播定位结果
    bool IsCalibFinished=false;
    ros::ServiceServer star_calib_axis,star_calib_one_pose,star_calib_run;
    std::string calibfilepath;
    std::string motorposetopic,lidartopic;
    std::string datapath,temppcdpath,initpcdpath;
    cv::Mat ir_lidar_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat current_map_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat init_pos_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat ndt_pose_exRT=cv::Mat::zeros(4, 4, CV_64F);
    pcl::PointCloud<pcl::PointXYZ> frame, map,neiborinmap,inputmap;
    ros::Subscriber sub_livox_msg;
    ros::Publisher pub_pose_status;
    int nframe,MaxFrames;
    float H_angle,V_angle;
    int Device_id,Task_id,Map_id;
    std::string plyname,mapFormat;
    ros::Time newpcd_time;
    std::vector<std::string>PLYfilenames;
    struct DataInfo
    {
        float H;
        float V;
        cv::Mat frame_map_exRT=cv::Mat::zeros(4, 4, CV_64F);
    };
    std::vector<DataInfo> DataInfo2H,DataInfo2V;
};





#endif