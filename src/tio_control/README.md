# tio_control
接收上位机或者网关层的消息,控制固定式建图/着色     
#  话题
##  运行模式
Topic: /tio/control/task_status  
Type: tio_control/task  
```json
int32 device_id               #设备 id
int32 task_id                 #task id
int32 map_id                  #map id
string task_name              #任务名称/mapping coloring
int32 task_code               #错误代码/mapping coloring
float32 task_progress         #任务进度
int32 task_status             #0:正常结束\1:终止\2:暂停\3:正在执行\4:未执行\5:超期\6:预执行\7:超时
string message                #任务当前信息
```

#  服务
##  开始建图任务
Topic: /tio/control/mapping_start  
Type: tio_control/mapping_task  
```json
int32 device_id
int32 task_id
int32 map_id
int32 flag
---
bool success
string message
```
##  控制建图任务
Topic: /tio/control/mapping_control  
Type: tio_control/control_task  
```json
int32 device_id
int32 task_id
int32 map_id
int32 flag
---
bool success
string message
```
##  开始着色任务
Topic: /tio/control/coloring_start  
Type: tio_control/coloring_task  
```json
int32 device_id
int32 task_id
int32 map_id
int32 flag
---
bool success
string message
```
##  控制着色任务
Topic: /tio/control/coloring_control  
Type: tio_control/control_task  
```json
int32 device_id
int32 task_id
int32 map_id
int32 flag
---
bool success
string message
```
#  启动
##  启动建图
```json
#driver driver
roslaunch livox_ros_driver livox_lidar.launch
roslaunch fixed_decoder visible_65.launch
roslaunch  fixed_yd_motor motor2.launch
#mapping control
roslaunch mapping_service mapping_service.launch
#control
roslaunch tio_control control.launch
#bridge
roslaunch tio_mqtt_bridge tio_bridge.launch
#ftp
roslaunch tio_ftp ftp.launch
```
##  启动着色
```json
#driver driver
roslaunch livox_ros_driver livox_lidar.launch
roslaunch fixed_hk_nosdk isapi_temp_68.launch
roslaunch  fixed_yd_motor motor2.launch
#tf
roslaunch tf_ircampose_service tf_ircampose_service.launch
#undistort
roslaunch undistort_service undistort_service.launch
#control
roslaunch tio_control control.launch
```

## Params
- control.launch  
```json
<launch>
  <node pkg="tio_control" type="tio_control_node" name="tio_control_node" output="screen" respawn="false" args="/home/li/log/tio_control/ 7">
      <param name="device_id" type="int" value="3"/>
      <param name="color_weight" type="int" value="5"/>
      <param name="motor_timeout" type="int" value="60"/>
      <param name="collect_timeout" type="int" value="120"/>
      <param name="mapping_color_timeout" type="int" value="300"/>
      <param name="finish_topic" type="str" value="/ydmsg/platform/keep"/>
      <param name="mapping_file" value="$(find tio_control)/launch/mapping.json"/>
      <param name="map_save_path" value="$(find mapping_service)/data/pcdFiles"/>
      <param name="coloring_finish_topic" type="str" value="/ydmsg/platform/keep"/>
      <param name="coloring_start_topic" type="str" value="/tio/coloring/params"/>
      <param name="coloring_timeout" type="int" value="300"/>
      <param name="upload_timeout" type="int" value="300"/>
  </node>
</launch>
```