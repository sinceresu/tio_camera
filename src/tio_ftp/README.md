# tio_ftp
ftp client upload file to ftp server     
#  话题
##  运行模式
Topic: /tio/ftp/status  
Type: tio_ftp/status  
```json
int32 device_id                 #设备id
int32 task_id                   #任务id
int32 map_id                    #地图id
float32 progress                #进度
int32 status                    #状态 1 成功 2 失败
string message                  #消息
```

#  服务
##  开始建图任务
Topic: /tio/ftp/upload  
Type: tio_ftp/mapping_task  
```json
int8 flag                       #1开始 2结束
int32 device_id                 #设备id
int32 task_id                   #任务id
int32 map_id                    #地图id
string map_version              #地图版本
string map_format               #地图格式
string src_name                 #需要上传的文件的全路径
string target_name              #要存放的文件名
---
bool success                 
string message
```
#  启动
##  启动
```json
roslaunch tio_ftp ftp.launch
```
##  启动说明
```json
<launch>
  <node pkg="tio_ftp" type="tio_ftp_node" name="tio_ftp_node" output="screen" respawn="false" args="/home/li/log/tio_ftp/ 7">
    <param name="ftp_host" type="str" value="192.168.1.35"/>
    <param name="ftp_port" type="int" value="21"/>
    <param name="ftp_username" type="str" value="ftp"/><!-- ftp username -->
    <param name="ftp_pwd" type="str" value=""/><!-- ftp password -->
    <param name="ftp_path" type="str" value="/file/input"/><!-- ftp ftp root path -->
  </node>
</launch>
```