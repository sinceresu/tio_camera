#include <blam_slam/BlamSlam.h>
#include <geometry_utils/Transform3.h>
#include <pcl/io/io.h>
#include <pcl/io/ply_io.h>
namespace gu = geometry_utils;

BlamSlam::BlamSlam()
{
}

BlamSlam::~BlamSlam() {}

void BlamSlam::yd_reset(){
    odometry_.yd_rest();
    mapper_.yd_reset();
    localization_.yd_reset();
}
bool BlamSlam::Initialize()
{
    if (!filter_.Initialize())
    {
        return false;
    }

    if (!odometry_.Initialize())
    {
        return false;
    }

    if (!localization_.Initialize())
    {
        return false;
    }
    if (!mapper_.Initialize())
    {
        return false;
    }
    return true;
}
void removeNANNNS1(pcl::PointCloud<pcl::PointXYZ> &in_pc,pcl::PointCloud<pcl::PointXYZ> &out_pc){
    std::vector<int> inliers_indicies;
    pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    for (int i = 0; i < in_cloud_ptr->points.size(); i++)
    {
        if(pcl_isfinite(in_cloud_ptr->points[i].x)&&pcl_isfinite(in_cloud_ptr->points[i].y)&&pcl_isfinite(in_cloud_ptr->points[i].z))
        {
            inliers_indicies.push_back(i);
        }
    }
    if(in_cloud_ptr->points.size()!=inliers_indicies.size()){
        std::cout << "------Remove1: " << in_cloud_ptr->points.size()-inliers_indicies.size() <<" nan points."<< std::endl;
    }
    pcl::copyPointCloud<pcl::PointXYZ>(in_pc, inliers_indicies, out_pc);
}

void BlamSlam::ProcessPointCloudMessage(const PointCloud::ConstPtr &msg)
{
    PointCloud::Ptr msg_filtered(new PointCloud);
    filter_.Filter(msg, msg_filtered); //一系列滤波
    //wxyadd下采样
    PointCloud::Ptr _filtered(new PointCloud);
    PointCloud::Ptr _filterednonone(new PointCloud);
    pcl::VoxelGrid<pcl::POINT_TYPE> grid;
    grid.setLeafSize(0.02, 0.02, 0.02);
    grid.setInputCloud(msg);
    grid.filter(*_filtered);
    //wxyadd平滑
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointNormal> mls_points;
    pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
    mls.setComputeNormals(true);
    mls.setInputCloud(_filtered);
    mls.setPolynomialFit(true);
    mls.setSearchMethod(tree);
    mls.setSearchRadius(0.05);
    mls.process(mls_points);
    pcl::PointCloud<pcl::POINT_TYPE> out_pc;
    for (pcl::PointCloud<pcl::PointNormal>::iterator pt = mls_points.begin();
         pt < mls_points.end(); pt++)
    {
        pcl::PointXYZ temp;
        temp.x = pt->x;
        temp.y = pt->y;
        temp.z = pt->z;
        out_pc.push_back(temp);
    }
    *_filtered = out_pc;
    std::cout<<"+++test log0: RemoveNans start.+++"<<std::endl;
    removeNANNNS1(*_filtered,*_filterednonone);
    std::cout<<"+++test log0: RemoveNans end.+++"<<std::endl;
//----------------------------------test----------------------------
    std::cout<<"+++test log1: NormalFilter start.+++"<<std::endl;
    //pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(out_pc));
    //wxyadd按照法线滤波
    pcl::StatisticalOutlierRemoval<pcl::POINT_TYPE> sor;
    sor.setInputCloud(_filterednonone);
    sor.setMeanK(5);		   //5 //设置在进行统计时考虑查询点邻近点数
    sor.setStddevMulThresh(1); //1 /设置判断是否为离群点的阈值
    sor.filter(*_filterednonone);

    std::vector<int> inliers_indicies;
    pcl::NormalEstimation<pcl::POINT_TYPE, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(_filterednonone);
    //normalEstimation.setRadiusSearch(0.5);
    normalEstimation.setKSearch(10);
    pcl::search::KdTree<pcl::POINT_TYPE>::Ptr kdtree(new pcl::search::KdTree<pcl::POINT_TYPE>);
    normalEstimation.setSearchMethod(kdtree);
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    normalEstimation.compute(*normals);
    for (int i = 0; i < normals->points.size(); i++)
    {
        Eigen::Vector3d v1(_filterednonone->points[i].x, _filterednonone->points[i].y, _filterednonone->points[i].z);
        Eigen::Vector3d v2(normals->points[i].normal_x, normals->points[i].normal_y, normals->points[i].normal_z);
        double cosValNew = v1.dot(v2) / (v1.norm() * v2.norm());
        if (abs(cosValNew) > 0.09f)
        {
            inliers_indicies.push_back(i);
        }
    }
    out_pc.resize(0);
    pcl::copyPointCloud<pcl::POINT_TYPE>(*_filterednonone, inliers_indicies, out_pc);
    std::cout<<"+++test log1: NormalFilter end.+++"<<std::endl;
//----------------------------------test----------------------------
    std::cout<<"+++test log2: RemoveNans start.+++"<<std::endl;
    *_filtered = out_pc;
    removeNANNNS1(*_filtered,*_filterednonone);
    std::vector<int> noUsed;
    pcl::removeNaNFromPointCloud(*_filterednonone, *_filterednonone, noUsed);
    std::cout<<"+++test log2: RemoveNans end.+++"<<std::endl;
    //IF  Fitst Frame ,do :
    if (!odometry_.UpdateEstimate(*_filterednonone))//第一次icp/ndt
    {
        std::cout<<"++++test first frame++++"<<std::endl;
        PointCloud::Ptr unused(new PointCloud);
        mapper_.InsertPoints(_filterednonone, unused.get());
        //loop_closure_.AddKeyScanPair(0, msg);
        return;
    }
    //ELSE
    PointCloud::Ptr msg_transformed(new PointCloud);
    PointCloud::Ptr msg_neighbors(new PointCloud);
    PointCloud::Ptr msg_base(new PointCloud);
    PointCloud::Ptr msg_fixed(new PointCloud);

    localization_.MotionUpdate(odometry_.GetIncrementalEstimate());//更新变换矩阵
    localization_.TransformPointsToFixedFrame(*_filterednonone, msg_transformed.get());//第一次对新点云进行变换得到msg_transformed
    mapper_.ApproxNearestNeighbors(*msg_transformed, msg_neighbors.get());//找临近点
    localization_.TransformPointsToSensorFrame(*msg_neighbors, msg_neighbors.get());//对临近点进行变换
    localization_.MeasurementUpdate(_filterednonone, msg_neighbors, msg_base.get());//临近点与新增点进行ndt细匹配
    localization_.MotionUpdate(gu::Transform3::Identity());
    localization_.TransformPointsToFixedFrame(*_filterednonone, msg_fixed.get());//新Frame坐标变换
    PointCloud::Ptr unused(new PointCloud);
    mapper_.InsertPoints(msg_fixed, unused.get());
}
std::string BlamSlam::itos(int i)
{
    std::stringstream s;
    s << i;
    return s.str();
}
