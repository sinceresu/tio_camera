#include "tool.h"
#include "scan.h"


Scan::Scan()
{
    ros::NodeHandle nh;
    ROS_INFO("start mapping_service");
    getParameters();
    //模块化相关
    collectstartservice = nh.advertiseService("/tio/map/mapping_start",  &Scan::start_mapping, this);//开始收集当前扇区数据
    collectstopservice = nh.advertiseService("/tio/map/mapping_stop",  &Scan::stop_mapping, this);//采集扇区数足够，buildmap结束，可以进行colormap
    collectquitservice = nh.advertiseService("/tio/map/mapping_quit",  &Scan::quit_mapping, this);//舍弃全部数据，终止当前任务
    pub_task_status=nh.advertise<mapping_msg::mapping_status>("/tio/map/mapping_status", 1);
    pub_task_progress=nh.advertise<mapping_msg::mapping_progress>("/tio/map/mapping_progress", 1);
    motorservice = nh.advertiseService("/tio/map/collect_map_data",  &Scan::motor_reached, this);
    //建图相关
    LivoxSLAM.Initialize();
    sub_livox_msg = nh.subscribe("/livox/lidar", 1, &Scan::RepubCbk, this); //转发livox消息并重新定义时间戳
    pub_pcl_out = nh.advertise<sensor_msgs::PointCloud2>("/yd_livox_points", 1);
    image_sub_color=nh.subscribe(colorcamtopic, 1, &Scan::RecordImgData, this);// 可见光
    pc_sub=nh.subscribe("/yd_livox_points", 1, &Scan::RecordPcdData, this);// Livox
    pub_nframes = nh.advertise<std_msgs::Int32>("/tio/collected_frame_cnt", 1);
    sub_nframes = nh.subscribe("/tio/collected_frame_cnt", 1, &Scan::CheckStatus, this);
    std::thread mapthd(std::bind(&Scan::BuildMap, this));
    mapthd.detach();
    std::thread colorthd(std::bind(&Scan::ColorMap, this));
    colorthd.detach();
    std::thread checkthd(std::bind(&Scan::CheckTopicTimeOut, this));
    checkthd.detach();
}
void Scan::RecordPcdData(const sensor_msgs::PointCloud2ConstPtr &laserCloudMsg)
{
    newpcd_time = ros::Time::now();
    /*once loop: scan->down->scan->left->scan->up->scan->left*/
    if (IsStop) //不采集
    {

    }
    else if(IsStart)//采集
    {
        //std::cout << "nframes:" << nframes << " MaxFrames:" << MaxFrames << " IsSavePcd:" << IsSavePcd << " IsSaveImg:" << IsSaveImg << std::endl;
        if (IsRecording && nframes < MaxFrames) //小于指定扫描密度帧数，继续添加数据
        {
            //save pcd
            pcl::PointCloud<pcl::PointXYZ> laserCloudIn;
            laserCloudIn.resize(0);
            pcl::fromROSMsg(*laserCloudMsg, laserCloudIn);
            newlaserCloudIn.insert(newlaserCloudIn.end(), laserCloudIn.begin(), laserCloudIn.end());
            nframes++;
        }
        else if (IsRecording&&nframes == MaxFrames&&IsSavePcd)
        {
            //save pcd
            int framenum = 100000 + nsector_cnt;
            std::string pcdname = int2str(framenum);
            std::cout<<"saved pcd"<<pcdname<<".pcd"<<std::endl;
            savelocal2(output_pcd_path, pcdname, newlaserCloudIn);
            newlaserCloudIn.resize(0);
            frameQueue.push(pcdname);
            if(frameQueue.size()!=angleQueue.size())
            {
                printlr("frameQueue.size()!=angleQueue.size()\n");
                std::cout<<"frameQueue.size()="<<frameQueue.size()<<" angleQueue.size()="<<angleQueue.size()<<std::endl;
            }
            IsSavePcd=false;
            IsRecording = false;
        }
    }
}
void Scan::RecordImgData(const sensor_msgs::ImageConstPtr &imageMsg_color)
{
    //ROS_INFO("---Recording Map Data---");
    newimg_time = ros::Time::now();
    /*once loop: scan->down->scan->left->scan->up->scan->left*/
    if (IsStop) //不采集
    {

    }
    else if(IsStart&&IsRecording&&nframes == MaxFrames&&IsSaveImg)//采集
    {
        //save image
        cv_bridge::CvImagePtr cv_ptr1 = cv_bridge::toCvCopy(imageMsg_color, sensor_msgs::image_encodings::TYPE_8UC3);
        cv::Mat img__1 = cv_ptr1->image;
        std::string saveimgpath1 = output_img_path + "color_" + int2str(name_base + nsector_cnt) + ".jpg";
        std::cout<<"saved color image"<<"color_"<<int2str(name_base + nsector_cnt)<<".jpg"<<std::endl;
        cv::imwrite(saveimgpath1, img__1);
        IsSaveImg = false;
        IsSavePcd = true;
    }
}
void Scan::CheckTopicTimeOut()
{
    while (1) {
        sleep(0.1);
        if (IsRecording&&(ros::Time::now()-newimg_time>ros::Duration(60.0f)||newimg_time-ros::Time::now()>ros::Duration(60.0f)))
        {
            mapping_msg::mapping_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="Unable to get image from color camera.";
            pub_task_status.publish(web_task_msg);
        }
        if (IsRecording&&(ros::Time::now()-newpcd_time>ros::Duration(60.0f)||newpcd_time-ros::Time::now()>ros::Duration(60.0f)))
        {
            mapping_msg::mapping_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="Unable to get pcd from livox lidar.";
            pub_task_status.publish(web_task_msg);
        }

    }
}
bool Scan::start_mapping(mapping_msg::start_mapping::Request &req,mapping_msg::start_mapping::Response &res)
{//全新的开始，参数设置，变量初始化,并无实际动作
    ROS_INFO("start_mapping recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl
            <<"resolution is "<<req.resolution_flag<<std::endl;
    Device_id=req.device_id;
    Task_id=req.task_id;
    Map_id=req.map_id;
    fout.open(pose_file_path);
    LivoxSLAM.yd_reset();
    if(req.resolution_flag==0){
        ROS_INFO("Set low resolution");
        MaxFrames=80;
        file_resolusion=0.017;
        res.success =true;
    }
    else if(req.resolution_flag==1){
        ROS_INFO("Set mid resolution");
        MaxFrames=120;
        file_resolusion=0.01;
        res.success =true;
    }
    else if(req.resolution_flag==2){
        ROS_INFO("Set high resolution");
        MaxFrames=200;
        file_resolusion=0.005;
        res.success =true;
    }
    else
    {
        ROS_INFO("Using defaut paramer from launch file.");
        res.success =true;
    }
    //常规启动
    mapping_msg::mapping_status web_task_msg;
    web_task_msg.device_id=Device_id;
    web_task_msg.task_id=Task_id;
    web_task_msg.map_id=Map_id;
    web_task_msg.task_status=0;//开始
    pub_task_status.publish(web_task_msg);
    IsStart=true;
    IsStop=false;
    IsQuit=false;
    IsSaveImg = false;
    while (!frameQueue.empty()) frameQueue.pop();
    while (!angleQueue.empty()) angleQueue.pop();
    while (!colorQueue.empty()) colorQueue.pop();
    return true;
}
bool Scan::motor_reached(mapping_msg::collect_data::Request &req,mapping_msg::collect_data::Response &res)
{
    ROS_INFO("motor_reached recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl
            <<"pitch_angle is "<<req.pitch_angle<<std::endl
            <<"roll_angle is "<<req.roll_angle<<std::endl;
    Device_id=req.device_id;
    Task_id=req.task_id;
    Map_id=req.map_id;
    res.success =true;
    res.message="recieve request collect data , please wait for topic \"/tio/collected_frame_cnt\".";
    cv::Point2f current_angle;
    current_angle.x=req.pitch_angle;//垂直角度
    current_angle.y=req.roll_angle;//水平角度
    angleQueue.push(current_angle);
    colorQueue.push(current_angle);

    sleep(1);
    IsRecording = true; //开启录制，该处严格受电机旋转状态控制
    nsector_cnt++;      //扇区加1
    nframes = 0;        //录制帧数清零
    IsSavePcd=false;
    IsSaveImg = true;
    return true;
}

bool Scan::stop_mapping(mapping_msg::stop_mapping::Request &req,mapping_msg::stop_mapping::Response &res)
{
    ROS_INFO("stop_mapping recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl
            <<"map_version is: "<<req.map_version<<std::endl
            <<"map_format is: "<<req.map_format<<std::endl;
    std::string plyname=req.map_version;
    std::string mapFormat=req.map_format;
    webresultsavedpath=CreatResultMapPath(Map_id)+plyname+"."+mapFormat;
    IsStop=true;//不会再有新的数据了
    IsSaveMap=true;
    mapping_msg::mapping_status web_task_msg;
    web_task_msg.device_id=Device_id;
    web_task_msg.task_id=Task_id;
    web_task_msg.map_id=Map_id;
    web_task_msg.task_status=3;//结束采集流程
    pub_task_status.publish(web_task_msg);
    res.success =true;
    res.message="recieve request stop mapping , no more new data will be collected, next render map.";
    return true;
}
bool Scan::quit_mapping(mapping_msg::quit_mapping::Request &req,mapping_msg::quit_mapping::Response &res)
{//清除一切，终止一切
    ROS_INFO("quit_mapping recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl;
    IsQuit=true;//唯一一处至true的地方
    ROS_INFO("Request Quit");
    newlaserCloudIn.resize(0);
    IsStop=true;
    nframes=0;
    IsStart=false;
    IsColor=false;
    nsector_cnt = -1;
    currentmap_cnt=0;
    datainfolist.resize(0);
    while (!frameQueue.empty()) frameQueue.pop();
    while (!angleQueue.empty()) angleQueue.pop();
    while (!colorQueue.empty()) colorQueue.pop();
    fout.close();
    mapping_msg::mapping_status web_task_msg;
    web_task_msg.device_id=Device_id;
    web_task_msg.task_id=Task_id;
    web_task_msg.map_id=Map_id;
    web_task_msg.task_status=2;//强制终止motor_reached
    pub_task_status.publish(web_task_msg);
    res.success =true;
    res.message="recieve request QUIT, clear all temp data.";
    return true;
}
void Scan::CheckStatus(const std_msgs::Int32::ConstPtr &nframes_)
{
    auto &_msg = nframes_;
    if (_msg->data == MaxFrames && IsStop)//正常结束当前扫描任务
    {
        ROS_INFO("---Scan Finished---");
    }
    else if (_msg->data == MaxFrames && IsQuit)//终止当前任务
    {
        ROS_INFO("---Quit current task---");
    }
}
void Scan::BuildMap()
{
    //check file exist
    pcl::visualization::CloudViewer viewer("Cloud Viewer");
    while (!viewer.wasStopped())
    {
        sleep(0.1);
        if (frameQueue.size() > 0 && angleQueue.size() > 0 && !IsQuit)
        {

            std::string filenumstr = frameQueue.front();
            std::cout << std::endl
                      << "InsertPoints: " << filenumstr << std::endl;
            frameQueue.pop();
            std::string pcdfilename = output_pcd_path + filenumstr + ".pcd";
            CheckPcdExist(filenumstr);
            pcl::PointCloud<pcl::PointXYZ> pcdfile;
            if (pcl::io::loadPCDFile<pcl::PointXYZ>(pcdfilename, pcdfile) == -1)
            {
                PCL_ERROR("Couldn't read pcd  \n");
                exit(1);
            }
            //init////
            cv::Point2f m_angle=angleQueue.front();
            angleQueue.pop();
            Eigen::Matrix4d transform = initMatrix(m_angle.x,m_angle.y);
            pcl::PointCloud<pcl::PointXYZ>::Ptr nearframes(new pcl::PointCloud<pcl::PointXYZ>());
            pcl::transformPointCloud(pcdfile, *nearframes, transform);
            //init end////
            try
            {
                LivoxSLAM.ProcessPointCloudMessage(nearframes);
                const gu::Transform3 estimate = LivoxSLAM.localization_.GetIntegratedEstimate();
                const Eigen::Matrix<double, 3, 3> R = estimate.rotation.Eigen();
                const Eigen::Matrix<double, 3, 1> T = estimate.translation.Eigen();
                Eigen::Matrix4d tf = Eigen::Matrix4d::Identity();
                tf.block(0, 0, 3, 3) = R;
                tf.block(0, 3, 3, 1) = T;
                gu::Vector3 EulerAngle;
                gu::Rot3 rot_mat(tf(0, 0), tf(0, 1), tf(0, 2),
                                 tf(1, 0), tf(1, 1), tf(1, 2),
                                 tf(2, 0), tf(2, 1), tf(2, 2));
                EulerAngle = rot_mat.GetEulerZYX();
                const Eigen::Matrix<double, 3, 1> EulerAngle_T = EulerAngle.Eigen();
                fout << filenumstr << " " << currentmap_cnt << " "
                     << "1"
                     << " " << tf(0, 3) << " " << tf(1, 3) << " " << tf(2, 3) << " " << EulerAngle_T(0, 0) << " " << EulerAngle_T(1, 0) << " " << EulerAngle_T(2, 0) << endl;
                std_msgs::Int32 msg;
                msg.data = nframes; //80
                nframes = 0;
                pub_nframes.publish(msg);
            }
            catch(Exception ex)
            {
                nsector_cnt--;
                colorQueue.pop();
                printlr("mapping faile, recollect current sector.\n");
                PCL_ERROR("Build map failed in %s \n",filenumstr);
                std_msgs::Int32 msg;
                msg.data = 0;
                nframes = 0;
                pub_nframes.publish(msg);
            }
            viewer.showCloud(LivoxSLAM.mapper_.map_data_);
        }
        else if (IsSaveMap && frameQueue.size() == 0 && angleQueue.size()==0)
        {
            IsSaveMap = false;
            pcl::PCDWriter writer;
            std::string outmapfile = output_pcd_path + "H_LiDAR_Map.pcd";
            pcl::PointCloud<pcl::PointXYZ>::Ptr msg_filtered(new pcl::PointCloud<pcl::PointXYZ>());
            //下采样
            pcl::VoxelGrid<pcl::POINT_TYPE> grid;
            grid.setLeafSize(0.01, 0.01, 0.01);
            grid.setInputCloud(LivoxSLAM.mapper_.map_data_);
            grid.filter(*msg_filtered);

            writer.write(outmapfile, *msg_filtered);
            ROS_INFO("---Map Finished---");
            IsColor = true;//唯一一处至true
            fout.close();
        }
    }
}
void Scan::ColorMap()
{
    while (1)
    {
        sleep(0.1);
        if(IsQuit)
        {
            IsColor = false;
            nframes=0;
            IsColor=false;
            //IsSaveMap=true;
            nsector_cnt = -1;
            currentmap_cnt=0;
            datainfolist.resize(0);
            while (!frameQueue.empty()) frameQueue.pop();
            while (!angleQueue.empty()) angleQueue.pop();
            while (!colorQueue.empty()) colorQueue.pop();
        }
        else if (IsColor)
        {
            ofstream imageposefout;
            std::string transformfile=output_img_path+"transformfile.txt";
            imageposefout.open(transformfile);
            std::cout << "Map Colorization Start..."<<std::endl;
            double dur;
            clock_t start, end;

            IsColor = false;
            //CheckPcdImgTxtExist();
            ReadColorCalibFile(extrcalib_path);
            getdatainfo(pose_file_path, datainfolist);
            cv::cv2eigen(color_lidar_exRT, inv_color_lidar_exRT);
            cv::cv2eigen(color_lidar_exRT, color_lidar_exRT_eigen);
            Eigen::Affine3d temp;
            temp.matrix() = inv_color_lidar_exRT;
            inv_color_lidar_exRT = temp.inverse().matrix();
            Sector_cnt = datainfolist.size();
            MaxFrames = datainfolist[0].stampname.size();
            std::cout << "There are " << Sector_cnt << " sectors, every sector has " << MaxFrames << " frames." << std::endl;
            //omp_set_num_threads(4);
            //#pragma omp parallel
            for (int i = 0; i < Sector_cnt; i++)
            {
                std::cout<<std::endl<<"+++test log: rendering "<<i<<" 'st frame start.+++"<<std::endl;
                //load and merge pointcloud in every sector
                pcl::PointCloud<pcl::PointXYZ> pc, pcfilternorm, pcfiltervox, pcfiltersmooth, pcfilter, unused_cloud, pcinrange, pcinrange1;
                pcl::PointCloud<pcl::PointXYZRGBL> sector_rgb;
                std::string framepcdname = output_pcd_path +  datainfolist[i].stampname[0] + ".pcd";
                if (pcl::io::loadPCDFile<pcl::PointXYZ>(framepcdname, pc) == -1)
                {
                    PCL_ERROR("Couldn't read pcd files\n");
                    exit(1);
                }
                //MovingLeast_filter(pcfiltervox,pcfiltersmooth);
                FrustumCulling_filter(pc, 40, 60, 1.0, 999, pcinrange);
                Point_Cloud_VoxelGrid_filter(pcinrange, maping_grid_res, pcfiltervox);
                Point_Cloud_Normal_filter(pcfiltervox, 0.05f, Normal_filter_thread, pcfilternorm);
                std::cout<<"------remove "<<pcfiltervox.size()-pcfilternorm.size()<<"normal points."<<std::endl;
                //Point_Cloud_Ror_filter(pcfilternorm, 0.05, 25, pcfilter);
                //load image;
                std::string imgname = output_img_path + "color_" + datainfolist[i].stampname[0] + ".jpg";
                int Lebal=str2int(datainfolist[i].stampname[0]);
                cv::Mat colorframe, colorundistortImg;
                colorframe = cv::imread(imgname, 1);
                colorundistortImg = colorframe.clone();
                if (distortion_matrix.size[0] == 4)
                {
                    cv::fisheye::undistortImage(colorframe, colorundistortImg, projection_matrix, distortion_matrix, projection_matrix);
                }
                else if (distortion_matrix.size[0] == 5)
                {
                    cv::undistort(colorframe, colorundistortImg, projection_matrix, distortion_matrix, projection_matrix);
                }
                std::cout<<"+++load image ok.+++"<<std::endl;
                //transform extrinsic
                pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud1(new pcl::PointCloud<pcl::PointXYZ>());
                pcl::transformPointCloud(pcfilternorm, *transformed_cloud1, inv_color_lidar_exRT); //雷达到可见光
                //Point_Cloud_PassThrough_filter("z", transformed_cloud1, 1.5, 100.0, pcinrange);
                char coloroutplyname[256];
                projectpointcloud(Lebal, *transformed_cloud1, projection_matrix, colorundistortImg, cv::Rect(0, 0, imagesize.width, imagesize.height), sector_rgb, &unused_cloud);
                std::cout<<"+++project color cloud ok.+++"<<std::endl;
                pcl::PointCloud<pcl::PointXYZRGBL>::Ptr transformed_cloud3(new pcl::PointCloud<pcl::PointXYZRGBL>());
                pcl::transformPointCloud(sector_rgb, *transformed_cloud3, color_lidar_exRT_eigen); //转回到雷达坐标系
                //init
                cv::Point2f m_angle=colorQueue.front();
                colorQueue.pop();
                Eigen::Matrix4d transform = initMatrix(m_angle.x,m_angle.y);
                //
                pcl::PointCloud<pcl::PointXYZRGBL>::Ptr init_cloud(new pcl::PointCloud<pcl::PointXYZRGBL>());
                pcl::transformPointCloud(*transformed_cloud3, *init_cloud, transform); //粗配位置
                //transform pose
                Eigen::Matrix<double, 4, 4> posetf = datainfolist[i].averagetf;
                pcl::transformPointCloud(*init_cloud, sector_rgb, posetf); //当前color帧转到第0帧原点
                //save each image pose
                cv::Mat poseRT,final_RT,averagetf_RT;
                cv::eigen2cv(transform, poseRT);
                poseRT.convertTo(poseRT, CV_64F);

                cv::eigen2cv(posetf, averagetf_RT);
                averagetf_RT.convertTo(averagetf_RT, CV_64F);
                Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();
                cv::cv2eigen(color_lidar_exRT, RT1_trans.matrix());
                cv::cv2eigen(poseRT, RT2_trans.matrix());
                cv::cv2eigen(averagetf_RT, RT3_trans.matrix());
                Eigen::Isometry3d delta = RT3_trans*RT2_trans*RT1_trans;
                cv::eigen2cv(delta.matrix(), final_RT);
                imageposefout << datainfolist[i].stampname[0]<< " "
                     << final_RT.at<double>(0, 0) << ", " << final_RT.at<double>(0, 1) << ", " << final_RT.at<double>(0, 2) << ", " << final_RT.at<double>(0, 3)
                     << ", " << final_RT.at<double>(1, 0) << ", " << final_RT.at<double>(1, 1) << ", " << final_RT.at<double>(1, 2) << ", " << final_RT.at<double>(1, 3)
                     << ", " << final_RT.at<double>(2, 0) << ", " << final_RT.at<double>(2, 1) << ", " << final_RT.at<double>(2, 2) << ", " << final_RT.at<double>(2, 3)
                     <<", "<< "0 0 0 1"<<std::endl;


                colorfinalpc.insert(colorfinalpc.end(), sector_rgb.begin(), sector_rgb.end());
                printProgress((double)(i + 1) / (double)Sector_cnt);
                mapping_msg::mapping_progress web_task_msg;
                web_task_msg.device_id=Device_id;
                web_task_msg.task_id=Task_id;
                web_task_msg.map_id=Map_id;
                web_task_msg.progress=(float)((float)((i + 1)) / (float)(Sector_cnt));//进度
                pub_task_progress.publish(web_task_msg);
                std::cout<<std::endl<<"+++test log: rendering "<<i<<" 'st frame end.+++"<<std::endl;
            }
            UniformSamplingfilter(colorfinalpc,file_resolusion, finalcolorpc);
           // RGB_Point_Cloud_VoxelGrid_filter(colorfinalpc, file_resolusion, finalcolorpc);
            std::cout<<std::endl;
            ROS_INFO("---Colorization Finished---");
            
            std::cout<<"The colorful map is wroten in:  "<<webresultsavedpath<<std::endl;
            pcl::io::savePLYFile(webresultsavedpath, finalcolorpc);
            finalcolorpc.resize(0);
            nframes=0;
            IsColor=false;
            IsSaveMap=false;
            nsector_cnt = -1;
            currentmap_cnt=0;
            datainfolist.resize(0);
            imageposefout.close();
            if(!frameQueue.empty()||!angleQueue.empty()||!colorQueue.empty())
            {
                printlr("!frameQueue.empty()||!angleQueue.empty()||!colorQueue.empty()\n");
                std::cout<<"frameQueue.size()="<<frameQueue.size()
                         <<" angleQueue.size()="<<angleQueue.size()
                         <<" colorQueue.size()="<<colorQueue.size()<<std::endl;
                //exit(1);
            }
            else
            {
                mapping_msg::mapping_status web_task_msg;
                web_task_msg.device_id=Device_id;
                web_task_msg.task_id=Task_id;
                web_task_msg.map_id=Map_id;
                web_task_msg.task_status=1;//正常结束
                pub_task_status.publish(web_task_msg);
            }
        }
    }
}
void Scan::projectpointcloud(int Lebal,
                             pcl::PointCloud<pcl::PointXYZ> point_cloud,
                             cv::Mat projection_matrix,
                             cv::Mat image,
                             cv::Rect frame,
                             pcl::PointCloud<pcl::PointXYZRGBL> &out_pc,
                             pcl::PointCloud<pcl::PointXYZ> *visible_points)
{
    int visible_size = 0;
    cv::Mat pt_3D(3, 1, CV_32FC1);
    projection_matrix.convertTo(projection_matrix, CV_32FC1);
    cv::Mat pt_2D;
    visible_points->clear();
    float fx = projection_matrix.at<float>(0, 0);
    float fy = projection_matrix.at<float>(1, 1);
    float cx = projection_matrix.at<float>(0, 2);
    float cy = projection_matrix.at<float>(1, 2);
    for (pcl::PointCloud<pcl::PointXYZ>::iterator pt = point_cloud.begin();
         pt < point_cloud.end(); pt++)
    {
        pcl::PointXYZRGBL temp;
        if (pt->z < 0)
            continue;
        pt_3D.at<float>(0) = pt->x;
        pt_3D.at<float>(1) = pt->y;
        pt_3D.at<float>(2) = pt->z;
        float u = pt->x * fx / pt->z + cx;
        float v = pt->y * fy / pt->z + cy;
        cv::Point xy(u, v);
        if (xy.inside(frame))
        {
            if (visible_points != NULL)
            {
                Vec3b vec_3 = image.at<Vec3b>(v, u);
                temp.x = pt->x;
                temp.y = pt->y;
                temp.z = pt->z;
                temp.r = vec_3[2];
                temp.g = vec_3[1];
                temp.b = vec_3[0];
                temp.label=Lebal;
                out_pc.push_back(temp);
                visible_points->push_back(*pt);
                visible_size++;
            }
        }
    }
}
void Scan::getdatainfo(std::string filename, std::vector<DataInfo> &infolist)
{
    double x, y, z, roll, yaw, pitch;
    std::string ts;
    int nsector_cnt, nframes;
    int allsector_sum = 0; //扇区数总数
    ifstream calib_File(filename);
    std::string line;
    while (std::getline(calib_File, line))
    {
        struct DataInfo oneinfo;
        std::istringstream iss(line);
        if (!(iss >> ts >> nsector_cnt >> nframes >> x >> y >> z >> roll >> pitch >> yaw)) { break; } // error
        oneinfo.stampname.push_back(ts);
        Eigen::Matrix<double, 3, 1> T;
        Eigen::Vector3d ea0(yaw, pitch, roll);
        Eigen::Matrix3d R;
        R = ::Eigen::AngleAxisd(ea0[0], ::Eigen::Vector3d::UnitZ()) * ::Eigen::AngleAxisd(ea0[1], ::Eigen::Vector3d::UnitY()) * ::Eigen::AngleAxisd(ea0[2], ::Eigen::Vector3d::UnitX());
        T << x, y, z;
        Eigen::Matrix4d tf = Eigen::Matrix4d::Identity();
        tf.block(0, 0, 3, 3) = R;
        tf.block(0, 3, 3, 1) = T;
        oneinfo.averagetf = tf;
        infolist.push_back(oneinfo);
    }
    calib_File.close();
}
void Scan::ReadColorCalibFile(std::string _choose_file)
{
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraExtrinsicMat"] >> color_lidar_exRT;
        fs["CameraMat"] >> projection_matrix;
        fs["DistCoeff"] >> distortion_matrix;
        fs["ImageSize"] >> imagesize;
    }
}
Eigen::Matrix4d Scan::initMatrix(float V_angle,float H_angle)
{
    cv::Mat init_exRT = cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat init_R = cv::Mat::zeros(3, 3, CV_64F);
    Eigen::Matrix3d rotation_matrix;
    double rad = 180.0f / M_PI;
    Eigen::Matrix4d transform;
    Eigen::AngleAxisd yawAngle, pitchAngle;
    yawAngle = Eigen::AngleAxisd(H_angle / rad, Eigen::Vector3d::UnitZ());
    pitchAngle = Eigen::AngleAxisd(V_angle / rad, Eigen::Vector3d::UnitY());
    rotation_matrix = yawAngle * pitchAngle;
    cv::eigen2cv(rotation_matrix, init_R);
    init_R.copyTo(init_exRT(cv::Rect_<double>(0, 0, 3, 3)));
    init_exRT.at<double>(0, 3) = 0.0;
    init_exRT.at<double>(1, 3) = 0.0;
    init_exRT.at<double>(2, 3) = 0.0;
    init_exRT.at<double>(3, 3) = 1.0;


    cv::cv2eigen(init_exRT, transform);
    return transform;
}
void Scan::CheckPcdExist(std::string filenumstr)
{
    std::string filename = output_pcd_path + filenumstr + ".pcd";
    if (FILE *file = fopen(filename.c_str(), "r"))
    {
        fclose(file);
    }
    else
    {
        std::cout << "can not read the " << filenumstr << ".pcd" << std::endl;
        exit(1);
    }
}
void Scan::CheckPcdImgTxtExist()
{
    std::cout<<"CheckPcdImgTxtExist Sector_cnt= "<<Sector_cnt<<std::endl;
    for (int filenum = 0; filenum < Sector_cnt; filenum++)
    {
        std::string name = int2str(filenum + 100000);
        std::string filename = output_pcd_path + name + ".pcd";
        if (FILE *file = fopen(filename.c_str(), "r"))
        {
            fclose(file);
        }
        else
        {
            std::cout << "can not read the " << name << ".pcd" << std::endl;
            exit(1);
        }
        filename = output_img_path + "color_" + name + ".jpg";
        if (FILE *file = fopen(filename.c_str(), "r"))
        {
            fclose(file);
        }
        else
        {
            std::cout << "can not read the color_" << name << ".jpg" << std::endl;
            exit(1);
        }
    }
    if (FILE *file = fopen(pose_file_path.c_str(), "r"))
    {
        fclose(file);
    }
    else
    {
        std::cout << "can not read the T_Matrix.txt" << std::endl;
        exit(1);
    }
}
void Scan::RepubCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in)
{
    pcl::PointCloud<pcl::PointXYZ> laserCloudIn, pcinrange;
    pcl::fromROSMsg(*livox_msg_in, laserCloudIn);
    for (auto pt = laserCloudIn.begin();
         pt < laserCloudIn.end(); pt++)
    {
        if (dis(*pt) > MinDistance && dis(*pt) < MaxDistance)
            pcinrange.push_back(*pt);
    }
    sensor_msgs::PointCloud2 pcl_ros_msg;
    pcl::toROSMsg(pcinrange, pcl_ros_msg);
    pcl_ros_msg.header.stamp = ros::Time::now();
    pcl_ros_msg.header.frame_id = "/livox";
    pub_pcl_out.publish(pcl_ros_msg);
}
