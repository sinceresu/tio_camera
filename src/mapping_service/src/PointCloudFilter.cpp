#include <point_cloud_filter/PointCloudFilter.h>
#include <parameter_utils/slamBase.h>
//#include <parameter_utils/ParameterUtils.h>

#include <pcl/filters/filter.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/random_sample.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/normal_3d.h>

//namespace pu = parameter_utils;

PointCloudFilter::PointCloudFilter() {}
PointCloudFilter::~PointCloudFilter() {}

bool PointCloudFilter::Initialize()
{
  if (!LoadParameters())
  {

    return false;
  }
  return true;
}

bool PointCloudFilter::LoadParameters()
{
  // Load filtering parameters.
  ParameterReader pd;
  params_.grid_filter = atoi(pd.getData("grid_filter").c_str());
  params_.grid_res = atof(pd.getData("grid_res").c_str());
  params_.random_filter = atoi(pd.getData("random_filter").c_str());
  params_.decimate_percentage = atof(pd.getData("decimate_percentage").c_str());
  params_.outlier_filter = atoi(pd.getData("outlier_filter").c_str());
  params_.outlier_std = atof(pd.getData("outlier_std").c_str());
  params_.outlier_knn = atoi(pd.getData("outlier_knn").c_str());

  params_.radius_filter = atoi(pd.getData("radius_filter").c_str());
  params_.radius = atof(pd.getData("radius").c_str());
  params_.radius_knn = atoi(pd.getData("radius_knn").c_str());

  // Cap to [0.0, 1.0].
  params_.decimate_percentage =
      std::min(1.0, std::max(0.0, params_.decimate_percentage));

  return true;
}

bool PointCloudFilter::Filter(const PointCloud::ConstPtr &points,
                              PointCloud::Ptr points_filtered) const
{
  if (points_filtered == NULL)
  {
    //  ROS_ERROR("%s: Output is null.", name_.c_str());
    return false;
  }

  // Copy input points.
  *points_filtered = *points;

  // 随机下采样滤波Apply a random downsampling filter to the incoming point cloud.
  if (params_.random_filter)
  { //0
    const int n_points = static_cast<int>((1.0 - params_.decimate_percentage) *
                                          points_filtered->size());
    pcl::RandomSample<pcl::POINT_TYPE> random_filter;
    random_filter.setSample(n_points);
    random_filter.setInputCloud(points_filtered);
    random_filter.filter(*points_filtered);
  }

  // 下采样滤波Apply a voxel grid filter to the incoming point cloud.
  if (params_.grid_filter)
  { //1
    pcl::VoxelGrid<pcl::POINT_TYPE> grid;
    grid.setLeafSize(params_.grid_res, params_.grid_res, params_.grid_res); //0.2
    grid.setInputCloud(points_filtered);
    grid.filter(*points_filtered);
  }

  // 平均距离一个标准差,移除离散点Remove statistical outliers in incoming the point cloud.
  if (params_.outlier_filter)
  { //1
    pcl::StatisticalOutlierRemoval<pcl::POINT_TYPE> sor;
    sor.setInputCloud(points_filtered);
    sor.setMeanK(params_.outlier_knn);           //5 //设置在进行统计时考虑查询点邻近点数
    sor.setStddevMulThresh(params_.outlier_std); //1 /设置判断是否为离群点的阈值
    sor.filter(*points_filtered);
  }

  // 按照半径内的点数，移除离散点Remove points without a threshold number of neighbors within a specified
  // radius.
  if (params_.radius_filter)
  { //0
    pcl::RadiusOutlierRemoval<pcl::POINT_TYPE> rad;
    rad.setInputCloud(points_filtered);
    rad.setRadiusSearch(params_.radius);
    rad.setMinNeighborsInRadius(params_.radius_knn);
    rad.filter(*points_filtered);
  }
  return true;
}
