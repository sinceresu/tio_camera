#ifndef SCAN_H
#define SCAN_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <time.h>
#include <queue>
//opencv
#include "eigen3/Eigen/Dense"
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
//pcl
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h> //kd-tree搜索对象的类定义的头文件
#include <pcl/surface/mls.h>         //最小二乘法平滑处理类定义头文件
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h> //NDT配准类对应头文件
#include <pclomp/ndt_omp.h>
#include <pcl_conversions/pcl_conversions.h>
//ros
#include <sensor_msgs/PointCloud2.h>
#include "livox_ros_driver/CustomMsg.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Image.h"
#include "nav_msgs/Odometry.h"
#include "mapping_msg/start_mapping.h"
#include "mapping_msg/stop_mapping.h"
#include "mapping_msg/quit_mapping.h"
#include "mapping_msg/collect_data.h"
#include "mapping_msg/mapping_progress.h"
#include "mapping_msg/mapping_status.h"
//slam
#include <blam_slam/BlamSlam.h>
#include <parameter_utils/slamBase.h>
//synchronism
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
//lib
#include "jsoncpp/json/json.h"
#include "jsoncpp/json/value.h"
#include "jsoncpp/json/writer.h"

using namespace cv;
using namespace std;
using namespace pcl;
class Scan
{
public:
    Scan();
private://web端控制相关
    int  Device_id,Task_id,Map_id;
    bool IsStart=false;       //true：即表示整个流程的开始
    bool IsQuit=false;        //true:表示舍弃当前所有已经扫描数据
    bool IsStop=true;         //false: 表示进行采集建图流程 true:采集数据完毕，可见光全点云进行着色
    ros::ServiceServer collectstartservice,collectstopservice,collectquitservice,motorservice;
    bool start_mapping(mapping_msg::start_mapping::Request &req,mapping_msg::start_mapping::Response &res);
    bool quit_mapping(mapping_msg::quit_mapping::Request &req,mapping_msg::quit_mapping::Response &res);
    bool motor_reached(mapping_msg::collect_data::Request &req,mapping_msg::collect_data::Response &res);
    bool stop_mapping(mapping_msg::stop_mapping::Request &req,mapping_msg::stop_mapping::Response &res);
    ros::Publisher pub_task_status,pub_task_progress;
    std::string webresultsavedpath;
private://建图及着色主要几个步骤
    void RepubCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in);
    void CheckStatus(const std_msgs::Int32::ConstPtr &nframes_);
    void RecordImgData(const sensor_msgs::ImageConstPtr &imageMsg_color);
    void RecordPcdData(const sensor_msgs::PointCloud2ConstPtr &laserCloudMsg);
    void CheckTopicTimeOut();
    void BuildMap();
    void ColorMap();
    void CheckPcdExist(std::string filenum);
    Eigen::Matrix4d initMatrix(float V_angle,float H_angle);

private://控制变量
    ros::Time newimg_time;
    ros::Time newpcd_time;
    ros::Subscriber sub_livox_msg,sub_nframes,image_sub_color,pc_sub;
    ros::Publisher pub_pcl_out, pub_nframes, pub_motorcmd; //转发点云数据、发送已采集帧数0-80、发送电机指令
    int name_base = 100000;
    int nframes = 0;
    int nsector_cnt = -1;     //等于0时进行初始点设置
    bool IsRecording = false; //true:旋转停止，可以录静止数据
    bool IsSaveImg = true;    //ture:可以采集图像数据，采集一张后false
    bool IsSavePcd=false;
    bool IsRotating = false;  //true:正在旋转
    bool IsSaveMap=false;
    bool IsColor=false;       //true:建图完毕，可进行着色


    pcl::PointCloud<pcl::PointXYZ> frame_pc, map_pc;
    pcl::PointCloud<pcl::PointXYZ> newlaserCloudIn;
    std::string pcdname;
    std::queue<std::string> frameQueue;
    std::queue<cv::Point2f> angleQueue;
    std::queue<cv::Point2f> colorQueue;
    BlamSlam LivoxSLAM;
    int currentmap_cnt=0;
    ofstream fout;
private:
    cv::Mat color_lidar_exRT, projection_matrix, distortion_matrix;
    cv::Size imagesize;
    Eigen::Matrix4d inv_color_lidar_exRT, color_lidar_exRT_eigen;
    pcl::PointCloud<pcl::PointXYZRGBL> colorfinalpc, finalcolorpc;
    struct DataInfo
    {
        std::vector<std::string> stampname;
        Eigen::Matrix4d averagetf;
    };
    std::vector<DataInfo> datainfolist;
private:
    void CheckPcdImgTxtExist();
    void ReadColorCalibFile(std::string _choose_file);
    void getdatainfo(std::string filename, std::vector<DataInfo> &infolist);
    void projectpointcloud(int Lebal,pcl::PointCloud<pcl::PointXYZ> point_cloud,
                           cv::Mat projection_matrix,
                           cv::Mat image,
                           cv::Rect frame,
                           pcl::PointCloud<pcl::PointXYZRGBL> &out_pc,
                           pcl::PointCloud<pcl::PointXYZ> *visible_points);
};

#endif
