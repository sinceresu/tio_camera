#include "ros/ros.h"

#include "scan.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "mapping_service");
  Scan scanor;
  ros::Rate loop_rate(10);
  while(ros::ok()){
    ros::spinOnce();
    loop_rate.sleep();
  }
  return 0;
}
