#include <point_cloud_mapper/PointCloudMapper.h>
#include <parameter_utils/slamBase.h>

//namespace pu = parameter_utils;

PointCloudMapper::PointCloudMapper()
        : initialized_(false),
          map_updated_(false),
          incremental_unsubscribed_(false) {
    // Initialize map data container.
    map_data_.reset(new PointCloud);
}

PointCloudMapper::~PointCloudMapper() {
    if (publish_thread_.joinable()) {
        publish_thread_.join();
    }
}

bool PointCloudMapper::Initialize(/*const ros::NodeHandle& n*/) {
    if (!LoadParameters()) {

        return false;
    }
    return true;
}

bool PointCloudMapper::LoadParameters() {
    ParameterReader pd;
    fixed_frame_id_ = pd.getData("fixed");
    map_data_->header.frame_id = fixed_frame_id_;
    octree_resolution_ = atof(pd.getData("octree_resolution").c_str());
    octree_resolution_ = 0.01f;
    // Initialize the map octree.
    map_octree_.reset(new Octree(octree_resolution_));
    map_octree_->setInputCloud(map_data_);

    initialized_ = true;

    return true;
}

void PointCloudMapper::Reset() {
    map_data_.reset(new PointCloud);
    map_data_->header.frame_id = fixed_frame_id_;
    map_octree_.reset(new Octree(octree_resolution_));
    map_octree_->setInputCloud(map_data_);
    initialized_ = true;
}
void PointCloudMapper::yd_reset(){
    map_updated_=false;
    incremental_unsubscribed_=false;
    map_data_.reset(new PointCloud);
    Reset();
}
bool PointCloudMapper::InsertPoints(const PointCloud::ConstPtr& points,
                                    PointCloud* incremental_points) {
    std::cout<<"+++test log6: PointCloudMapper::InsertPoints start..+++"<<std::endl;
    if (!initialized_) {
        return false;
    }

    if (incremental_points == NULL) {
        return false;
    }
    incremental_points->clear();

    if (map_mutex_.try_lock()) {

        for (size_t ii = 0; ii < points->points.size(); ++ii) {
            const pcl::POINT_TYPE p = points->points[ii];
            double min_x, min_y, min_z, max_x, max_y, max_z;
            map_octree_->getBoundingBox(min_x, min_y, min_z, max_x, max_y, max_z);
            bool isInBox = (p.x >= min_x && p.x <= max_x) && (p.y >= min_y && p.y <= max_y) && (p.z >= min_z && p.z <= max_z);

            if (!isInBox || !map_octree_->isVoxelOccupiedAtPoint(p)) {
                //if (!map_octree_->isVoxelOccupiedAtPoint(p)) {
                map_octree_->addPointToCloud(p, map_data_);
                incremental_points->push_back(p);
            }
        }
        map_mutex_.unlock();
    } else {

    }

    incremental_points->header = points->header;
    incremental_points->header.frame_id = fixed_frame_id_;

    map_updated_ = true;
    std::cout<<"+++test log6: PointCloudMapper::InsertPoints end..+++"<<std::endl;
    return true;
}

bool PointCloudMapper::ApproxNearestNeighbors(const PointCloud& points,
                                              PointCloud* neighbors) {
    std::cout<<"+++test log3: RPointCloudMapper::ApproxNearestNeighbors start.+++"<<std::endl;
    if (!initialized_) {
    }

    if (neighbors == NULL) {
    }

    neighbors->points.clear();
    std::cout<<"+++++++map_octree_->approxNearestSearch "<<points.points.size()<<"->"<<std::endl;
    for (size_t ii = 0; ii < points.points.size(); ++ii) {
        float unused = 0.f;
        int result_index = -1;
        map_octree_->approxNearestSearch(points.points[ii], result_index, unused);
        if (result_index >= 0)
            neighbors->push_back(map_data_->points[result_index]);
    }
    std::cout<<neighbors->points.size()<<std::endl;
    std::cout<<"+++test log3: RPointCloudMapper::ApproxNearestNeighbors end.+++"<<std::endl;
    return neighbors->points.size() > 0;
}