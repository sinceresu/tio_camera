#include <point_cloud_localization/PointCloudLocalization.h>
#include <parameter_utils/slamBase.h>
//#include <parameter_utils/ParameterUtils.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h> //NDT配准类对应头文件
#include <pclomp/ndt_omp.h>
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
using namespace cv;
namespace gu = geometry_utils;
//namespace gr = gu::ros;
//namespace pu = parameter_utils;

using pcl::GeneralizedIterativeClosestPoint;
using pcl::PointCloud;
using pcl::POINT_TYPE;
using pcl::transformPointCloud;

PointCloudLocalization::PointCloudLocalization() {}
PointCloudLocalization::~PointCloudLocalization() {}
void PointCloudLocalization::yd_reset() {
    //std::cout<<"+++++test PointCloudLocalization::yd_reset+++++++"<<incremental_estimate_<<integrated_estimate_<<std::endl;
    incremental_estimate_=gu::Transform3::Identity();
    integrated_estimate_=gu::Transform3::Identity();
    //std::cout<<"+++++test PointCloudLocalization::yd_reset+++++++"<<incremental_estimate_<<integrated_estimate_<<std::endl;
}
bool PointCloudLocalization::Initialize() {

    if (!LoadParameters()) {

        return false;
    }
    return true;
}

bool PointCloudLocalization::LoadParameters() {

    ParameterReader pd;
    fixed_frame_id_ = pd.getData("fixed");
    base_frame_id_ = pd.getData("base");
    // Load initial position.
    double init_x = 0.0, init_y = 0.0, init_z = 0.0;
    double init_roll = 0.0, init_pitch = 0.0, init_yaw = 0.0;
    init_x = atof(pd.getData("position_x").c_str());
    init_y = atof(pd.getData("position_y").c_str());
    init_z = atof(pd.getData("position_z").c_str());

    init_roll = atof(pd.getData("orientation_roll").c_str());
    init_pitch = atof(pd.getData("orientation_pitch").c_str());
    init_yaw = atof(pd.getData("orientation_yaw").c_str());
    integrated_estimate_.translation = gu::Vec3(init_x, init_y, init_z);
    integrated_estimate_.rotation = gu::Rot3(init_roll, init_pitch, init_yaw);

    params_.tf_epsilon = atof(pd.getData("tf_epsilon").c_str());
    params_.corr_dist = atof(pd.getData("corr_dist").c_str());
    params_.iterations = atoi(pd.getData("iterations").c_str());

    transform_thresholding_ = atoi(pd.getData("transform_thresholding").c_str());
    max_translation_ = atof(pd.getData("max_translation").c_str());
    max_rotation_ = atof(pd.getData("max_rotation").c_str());

    return true;
}


const gu::Transform3& PointCloudLocalization::GetIncrementalEstimate() const {
    return incremental_estimate_;
}

const gu::Transform3& PointCloudLocalization::GetIntegratedEstimate() const {
    return integrated_estimate_;
}

void PointCloudLocalization::SetIntegratedEstimate(
        const gu::Transform3& integrated_estimate) {
    integrated_estimate_ = integrated_estimate;

}

bool PointCloudLocalization::MotionUpdate(
        const gu::Transform3& incremental_odom) {
    // Store the incremental transform from odometry.
    incremental_estimate_ = incremental_odom;
    return true;
}

bool PointCloudLocalization::TransformPointsToFixedFrame(
        const PointCloud& points, PointCloud* points_transformed) const {
    if (points_transformed == NULL) {
        return false;
    }

    // Compose the current incremental estimate (from odometry) with the
    // integrated estimate, and transform the incoming point cloud.
    const gu::Transform3 estimate =
            gu::PoseUpdate(integrated_estimate_, incremental_estimate_);
    const Eigen::Matrix<double, 3, 3> R = estimate.rotation.Eigen();
    const Eigen::Matrix<double, 3, 1> T = estimate.translation.Eigen();

    Eigen::Matrix4d tf;
    tf.block(0, 0, 3, 3) = R;
    tf.block(0, 3, 3, 1) = T;

    pcl::transformPointCloud(points, *points_transformed, tf);

    return true;
}

bool PointCloudLocalization::TransformPointsToSensorFrame(
        const PointCloud& points, PointCloud* points_transformed) const {
    if (points_transformed == NULL) {
        return false;
    }

    // Compose the current incremental estimate (from odometry) with the
    // integrated estimate, then invert to go from world to sensor frame.
    const gu::Transform3 estimate = gu::PoseInverse(
            gu::PoseUpdate(integrated_estimate_, incremental_estimate_));//integrated_estimate from config parameters
    const Eigen::Matrix<double, 3, 3> R = estimate.rotation.Eigen();
    const Eigen::Matrix<double, 3, 1> T = estimate.translation.Eigen();

    Eigen::Matrix4d tf;
    tf.block(0, 0, 3, 3) = R;
    tf.block(0, 3, 3, 1) = T;

    pcl::transformPointCloud(points, *points_transformed, tf);

    return true;
}
void removeNANNNS(pcl::PointCloud<pcl::PointXYZ> &in_pc,pcl::PointCloud<pcl::PointXYZ> &out_pc){
    std::vector<int> inliers_indicies;
    pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    for (int i = 0; i < in_cloud_ptr->points.size(); i++)
    {
        if(pcl_isfinite(in_cloud_ptr->points[i].x)&&pcl_isfinite(in_cloud_ptr->points[i].y)&&pcl_isfinite(in_cloud_ptr->points[i].z))
        {
            inliers_indicies.push_back(i);
        }
    }
    if(in_cloud_ptr->points.size()!=inliers_indicies.size()){
        cout << "Remove: " << in_cloud_ptr->points.size()-inliers_indicies.size() <<" nan points."<< std::endl;
    }
    pcl::copyPointCloud<pcl::PointXYZ>(in_pc, inliers_indicies, out_pc);

}
bool PointCloudLocalization::MeasurementUpdate(const PointCloud::Ptr& query,
                                               const PointCloud::Ptr& reference,
                                               PointCloud* aligned_query) {
    if (aligned_query == NULL) {
        return false;
    }
    double rad = 180.0f / M_PI;
    pcl::PointCloud<pcl::PointXYZ>::Ptr p1(new pcl::PointCloud<pcl::PointXYZ>());
    pcl::PointCloud<pcl::PointXYZ>::Ptr p2(new pcl::PointCloud<pcl::PointXYZ>());
    std::cout<<"+++test log4: RemoveNans start..+++"<<std::endl;
    removeNANNNS(*query,*p1);
    removeNANNNS(*reference,*p2);
    std::vector<int> noUsed;
    pcl::removeNaNFromPointCloud(*p1, *p1, noUsed);
    pcl::removeNaNFromPointCloud(*p2, *p2, noUsed);
    std::cout<<"+++test log4: RemoveNans end.+++"<<std::endl;
////ndt wxy add
    std::cout<<"+++test log5: ndt new frame and neighbors start..+++"<<std::endl;
    pclomp::NormalDistributionsTransform<pcl::POINT_TYPE, pcl::POINT_TYPE>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::POINT_TYPE, pcl::POINT_TYPE>());
    ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
    ndt->setNumThreads(omp_get_max_threads());
    ndt->setTransformationEpsilon(0.0000001);
    ndt->setStepSize(0.1);
    ndt->setResolution(0.05);//匹配单元的最小立方体边长，要保证立方体中的点云数量最够多
    ndt->setMaximumIterations(35);
    ndt->setInputSource(p1);   //第二次扫描的点云进行体素滤波的结果.
    ndt->setInputTarget(p2); //第一次扫描的结果
    pcl::PointCloud<pcl::POINT_TYPE>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();
    ndt->align(*output_cloud, init_guess);
    //std::cout << " ndt score: " << ndt->getFitnessScore() << std::endl;
    Eigen::Matrix4f T = ndt->getFinalTransformation();
    cv::Mat ndt_matrix=cv::Mat::zeros(4, 4, CV_64F);
    cv::eigen2cv(T, ndt_matrix);
    Eigen::Matrix3d rotation_matrix;
    cv::Mat handin_R = ndt_matrix(cv::Rect(0, 0, 3, 3));
    cv::Mat handin_T = ndt_matrix(cv::Rect(3, 0, 1, 3));
    cv::cv2eigen(handin_R, rotation_matrix);
    Eigen::Vector3d eulerAngle = rotation_matrix.eulerAngles(2, 1, 0);
    std::cout << " ndt score: " << ndt->getFitnessScore()<<std::endl;
////ndt end
    pcl::transformPointCloud(*query, *aligned_query, T);
    gu::Transform3 pose_update;
    pose_update.translation = gu::Vec3(T(0, 3), T(1, 3), T(2, 3));
    pose_update.rotation = gu::Rot3(T(0, 0), T(0, 1), T(0, 2),
                                    T(1, 0), T(1, 1), T(1, 2),
                                    T(2, 0), T(2, 1), T(2, 2));

    // Only update if the transform is small enough.
    if (!transform_thresholding_ ||
        (pose_update.translation.Norm() <= max_translation_ &&
         pose_update.rotation.ToEulerZYX().Norm() <= max_rotation_)) {
        incremental_estimate_ = gu::PoseUpdate(incremental_estimate_, pose_update);
    } else {

    }

    integrated_estimate_ =
            gu::PoseUpdate(integrated_estimate_, incremental_estimate_);

    std::cout<<"+++test log5: ndt new frame and neighbors end..+++"<<std::endl;
    return true;
}
