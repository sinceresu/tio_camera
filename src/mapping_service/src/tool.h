#ifndef TOOL_H
#define TOOL_H
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <time.h>
#include "omp.h"
//opencv
#include "eigen3/Eigen/Dense"
#include <opencv2/opencv.hpp>
#include <opencv2/core/eigen.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
//pcl
#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/frustum_culling.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/approximate_voxel_grid.h>
#include <pcl/kdtree/kdtree_flann.h> //kd-tree搜索对象的类定义的头文件
#include <pcl/surface/mls.h>         //最小二乘法平滑处理类定义头文件
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h> //NDT配准类对应头文件
#include <pclomp/ndt_omp.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/keypoints/uniform_sampling.h>//均匀采样
//ros
#include <sensor_msgs/PointCloud2.h>
#include "livox_ros_driver/CustomMsg.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Image.h"
#include "nav_msgs/Odometry.h"
//slam
#include <blam_slam/BlamSlam.h>
#include <parameter_utils/slamBase.h>
//synchronism
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#define BEIJINGTIME 8
#define DAY        (60*60*24)
#define YEARFIRST  2001
#define YEARSTART  (365*(YEARFIRST-1970) + 8)
#define YEAR400    (365*4*100 + (4*(100/4 - 1) + 1))
#define YEAR100    (365*100 + (100/4 - 1))
#define YEAR004    (365*4 + 1)
#define YEAR001    365
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define L_GREEN    "\e[1;32m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)
#define printlg(format, arg...) do{printf(L_GREEN format NONE,## arg);}while(0)

using namespace std;
using namespace sensor_msgs;
using namespace message_filters;
namespace gu = geometry_utils;
typedef pcl::PointXYZINormal PointType;
ros::ServiceClient cloudplatform_client;
std::string output_pcd_path, pose_file_path, output_img_path, parameter_path, globalmap_path, extrcalib_path,colorcamtopic;
float maping_grid_res,Normal_filter_thread,file_resolusion;
#define PBSTR "||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||"
#define PBWIDTH 60
int MaxFrames, ResetOrigin, Sector_cnt, professmode, Vcnt, Hcnt;
double Rot_H_step, Rot_V_step;
int roll_angle=0,pitch_angle=0;
float normal_threshold, MinDistance, MaxDistance;

string double2str(double num);
string int2str(int num);
string long2str(long num);
string float2str(float num);

float dis(pcl::PointXYZ myPoint);
void getParameters();
void ReadCalibrationFile(std::string _choose_file, cv::Mat &out_RT);
void printProgress(double percentage);
void savelocal2(std::string output_pcd_path, std::string filename, pcl::PointCloud<pcl::PointXYZ> &vector_data);
void Point_Cloud_Ror_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc, float radius, int min_pts, pcl::PointCloud<pcl::PointXYZ> &out_pc);
void Point_Cloud_VoxelGrid_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc, float radius, pcl::PointCloud<pcl::PointXYZ> &out_pc);
void M4dtoM4f(Eigen::Matrix4d &transformation, Eigen::Matrix4f &trans_d);
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d);
void UniformSamplingfilter(pcl::PointCloud<pcl::PointXYZRGBL> &in_pc,float radius,pcl::PointCloud<pcl::PointXYZRGBL> &out_pc)
{
std::cout<<"+++test log7: UniformSamplingfilter start.+++"<<std::endl;
    
    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBL>(in_pc));
    // pcl::PointCloud<pcl::PointXYZRGB>::Ptr filteredCloud(new pcl::PointCloud<pcl::PointXYZRGB>);

    pcl::UniformSampling<pcl::PointXYZRGBL> filter;
    filter.setInputCloud(cloud);
    filter.setRadiusSearch(radius);
    // We need an additional object to store the indices of surviving points.
    pcl::PointCloud<int> keypointIndices;

    filter.filter(out_pc);
    //pcl::copyPointCloud(*cloud, keypointIndices.points, out_pc);
std::cout<<"+++test log7: UniformSamplingfilter end.+++"<<std::endl;

}
void getParameters()
{
  cout << "Get the parameters from the launch file" << endl;
  if (!ros::param::get("output_pcd_path", output_pcd_path))
  {
    cout << "Can not get the value of output_pcd_path" << endl;
    exit(1);
  }
  if (!ros::param::get("output_img_path", output_img_path))
  {
    cout << "Can not get the value of output_img_path" << endl;
    exit(1);
  }
  if (!ros::param::get("pose_file_path", pose_file_path))
  {
    cout << "Can not get the value of pose_file_path" << endl;
    exit(1);
  }
  if (!ros::param::get("MaxFrames", MaxFrames))
  {
    cout << "Can not get the value of MaxFrames" << endl;
    exit(1);
  }
  if (!ros::param::get("H_Angel", Rot_H_step))
  {
    cout << "Can not get the value of Rot_H_step" << endl;
    exit(1);
  }
  if (!ros::param::get("V_Angel", Rot_V_step))
  {
    cout << "Can not get the value of Rot_V_step" << endl;
    exit(1);
  }
  if (!ros::param::get("ResetOrigin", ResetOrigin))
  {
    cout << "Can not get the value of ResetOrigin" << endl;
    exit(1);
  }
  if (!ros::param::get("Hcnt", Hcnt))
  {
    cout << "Can not get the value of Sector_cnt" << endl;
    exit(1);
  }
  if (!ros::param::get("Vcnt", Vcnt))
  {
    cout << "Can not get the value of Vcnt_in_Sector" << endl;
    exit(1);
  }
  Sector_cnt = (Hcnt + 1) * (Vcnt + 1) - 1;
  if (!ros::param::get("globalmap_path", globalmap_path))
  {
    cout << "Can not get the value of globalmap_path" << endl;
    exit(1);
  }
  if (!ros::param::get("extrcalib_path", extrcalib_path))
  {
    cout << "Can not get the value of extrcalib_path" << endl;
    exit(1);
  }
  if (!ros::param::get("MinDistance", MinDistance))
  {
    cout << "Can not get the value of MinDistance" << endl;
    exit(1);
  }
  if (!ros::param::get("MaxDistance", MaxDistance))
  {
    cout << "Can not get the value of MaxDistance" << endl;
    exit(1);
  }
  if (!ros::param::get("maping_grid_res", maping_grid_res))
  {
    cout << "Can not get the value of maping_grid_res" << endl;
    exit(1);
  }
  if (!ros::param::get("file_resolusion", file_resolusion))
  {
    cout << "Can not get the value of file_resolusion" << endl;
    exit(1);
  }
  if (!ros::param::get("Normal_filter_thread", Normal_filter_thread))
  {
    cout << "Can not get the value of Normal_filter_thread" << endl;
    exit(1);
  }
    if (!ros::param::get("colorcamtopic", colorcamtopic))
  {
    cout << "Can not get the value of colorcamtopic" << endl;
    exit(1);
  }
}

void printProgress(double percentage)
{
  int val = (int)(percentage * 100);
  int lpad = (int)(percentage * PBWIDTH);
  int rpad = PBWIDTH - lpad;
  printf("\r%3d%% [%.*s%*s]", val, lpad, PBSTR, rpad, "");
  fflush(stdout);
}
float dis(pcl::PointXYZ myPoint)
{
  return sqrt(myPoint.x * myPoint.x + myPoint.y * myPoint.y + myPoint.z * myPoint.z);
}

void savelocal2(std::string output_pcd_path, std::string filename, pcl::PointCloud<pcl::PointXYZ> &vector_data)
{
  std::string outpcdname = output_pcd_path + filename + ".pcd";
  //std::cout << "saving pcd file into: " << outpcdname << std::endl;
  pcl::io::savePCDFile(outpcdname, vector_data);
}
void RGB_Point_Cloud_Ror_filter(pcl::PointCloud<pcl::PointXYZRGB> &in_pc,
                            float radius, int min_pts,
                            pcl::PointCloud<pcl::PointXYZRGB> &out_pc)
{
  std::vector<int> inliers_indicies;
  pcl::PointCloud<pcl::PointXYZRGB>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>(in_pc));
  pcl::RadiusOutlierRemoval<pcl::PointXYZRGB> pcFilter;
  pcFilter.setInputCloud(in_cloud_ptr);
  pcFilter.setRadiusSearch(radius);
  pcFilter.setMinNeighborsInRadius(min_pts);
  pcFilter.filter(inliers_indicies);
  pcl::copyPointCloud<pcl::PointXYZRGB>(in_pc, inliers_indicies, out_pc);
}
void Point_Cloud_Ror_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc,
                            float radius, int min_pts,
                            pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
  std::vector<int> inliers_indicies;
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
  pcl::RadiusOutlierRemoval<pcl::PointXYZ> pcFilter;
  pcFilter.setInputCloud(in_cloud_ptr);
  pcFilter.setRadiusSearch(radius);
  pcFilter.setMinNeighborsInRadius(min_pts);
  pcFilter.filter(inliers_indicies);
  pcl::copyPointCloud<pcl::PointXYZ>(in_pc, inliers_indicies, out_pc);
}
void RGB_Point_Cloud_VoxelGrid_filter(pcl::PointCloud<pcl::PointXYZRGB> &in_pc, //下采样
                                  float radius,
                                  pcl::PointCloud<pcl::PointXYZRGB> &out_pc)
{
    std::cout<<"+++test log7: RGB_Point_Cloud_VoxelGrid_filter start.+++"<<std::endl;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZRGB>(in_pc));
    pcl::VoxelGrid<pcl::PointXYZRGB> sor;
    sor.setInputCloud(in_cloud_ptr);
    sor.setLeafSize(radius, radius, radius);
    sor.filter(out_pc);
    std::cout<<"+++test log7: RGB_Point_Cloud_VoxelGrid_filter end.+++"<<std::endl;
}
void Point_Cloud_VoxelGrid_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc,
                                  float radius,
                                  pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
  pcl::VoxelGrid<pcl::PointXYZ> sor;
  sor.setInputCloud(in_cloud_ptr);
  sor.setLeafSize(radius, radius, radius);
  sor.filter(out_pc);
}
void MovingLeast_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc, pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
  // 输出文件中有PointNormal类型，用来存储移动最小二乘法算出的法线
  pcl::PointCloud<pcl::PointNormal> mls_points;
  // 定义对象 (第二种定义类型是为了存储法线, 即使用不到也需要定义出来)
  pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointNormal> mls;
  mls.setComputeNormals(true); //设置在最小二乘计算中需要进行法线估计
  //设置参数
  mls.setInputCloud(cloud);
  mls.setPolynomialFit(true);
  mls.setSearchMethod(tree);
  mls.setSearchRadius(0.1);
  // 曲面重建
  mls.process(mls_points);
  for (pcl::PointCloud<pcl::PointNormal>::iterator pt = mls_points.begin();
       pt < mls_points.end(); pt++)
  {
    pcl::PointXYZ temp;
    temp.x = pt->x;
    temp.y = pt->y;
    temp.z = pt->z;
    out_pc.push_back(temp);
  }
}
void Point_Cloud_Normal_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc,
                               float radius, float normalangle,
                               pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    float N = cos(normalangle / 180.0 * M_PI);
    //std::cout << "N= " << N << std::endl;
    std::vector<int> inliers_indicies;
    pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normalEstimation;
    normalEstimation.setInputCloud(in_cloud_ptr);
    normalEstimation.setRadiusSearch(radius);
    //normalEstimation.setKSearch(3);
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree(new pcl::search::KdTree<pcl::PointXYZ>);
    normalEstimation.setSearchMethod(kdtree);
    pcl::PointCloud<pcl::Normal>::Ptr normals(new pcl::PointCloud<pcl::Normal>);
    normalEstimation.compute(*normals);

    for (int i = 0; i < normals->points.size(); i++)
    {
        Eigen::Vector3d v1(in_cloud_ptr->points[i].x, in_cloud_ptr->points[i].y, in_cloud_ptr->points[i].z);
        Eigen::Vector3d v2(normals->points[i].normal_x, normals->points[i].normal_y, normals->points[i].normal_z);
        double cosValNew = v1.dot(v2) / (v1.norm() * v2.norm()); //角度cos值
                                                                 //double angleNew = acos(cosValNew) * 180 / M_PI;     //弧度角
        if (abs(cosValNew) > N)
        {
            inliers_indicies.push_back(i);
        }
    }
    //cout << "Point_Cloud_Normal_filter.nliers_indicies.size(): " << inliers_indicies.size() << std::endl;
    pcl::copyPointCloud<pcl::PointXYZ>(in_pc, inliers_indicies, out_pc);
}
void FrustumCulling_filter(pcl::PointCloud<pcl::PointXYZ> &in_pc,
                           float VerticalFOV, float HorizontalFOV,
                           float NearPlaneDistance, float FarPlaneDistance,
                           pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr source(new pcl::PointCloud<pcl::PointXYZ>(in_pc));
    pcl::FrustumCulling<pcl::PointXYZ> fc;
    fc.setInputCloud(source);
    fc.setVerticalFOV(VerticalFOV);
    fc.setHorizontalFOV(HorizontalFOV);
    fc.setNearPlaneDistance(NearPlaneDistance);
    fc.setFarPlaneDistance(FarPlaneDistance);
    Eigen::Matrix4f camera_pose(Eigen::Matrix4f::Identity());
    Eigen::Affine3f transf;
    pcl::getTransformation(0, 0, 0, 0, 0, 0, transf);
    camera_pose = transf.matrix();
    fc.setCameraPose(camera_pose);
    fc.filter(out_pc);
}

void Point_Cloud_PassThrough_filter(std::string Axis, pcl::PointCloud<pcl::PointXYZ>::Ptr in_pc,
                                    float limit_min, float limit_max,
                                    pcl::PointCloud<pcl::PointXYZ> &out_pc)
{
    std::vector<int> inliers_indicies;
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(in_pc);
    pass.setFilterFieldName(Axis);
    pass.setFilterLimits(limit_min, limit_max);
    pass.filter(inliers_indicies);
    pcl::copyPointCloud<pcl::PointXYZ>(*in_pc, inliers_indicies, out_pc);
}
double NDT_Process(pcl::PointCloud<pcl::PointXYZ> &Input, pcl::PointCloud<pcl::PointXYZ> &Target, Eigen::Matrix4f &init_guess, Eigen::Matrix4d &trans_d)
{
  pcl::PointCloud<pcl::PointXYZ>::Ptr Inputpt(new pcl::PointCloud<pcl::PointXYZ>(Input));    //待匹配的点云
  pcl::PointCloud<pcl::PointXYZ>::Ptr Targetptr(new pcl::PointCloud<pcl::PointXYZ>(Target)); //地图点云
  pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
  ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
  ndt->setNumThreads(omp_get_max_threads());
  //设置依赖尺度NDT参数
  //为终止条件设置最大转换差异
  ndt->setTransformationEpsilon(0.01);
  //为More-Thuente线搜索设置最大步长
  ndt->setStepSize(0.1);
  //网格大小设置在NDT中非常重要，太大会导致精度不高，太小导致内存过高
  ndt->setResolution(0.05);
  //设置匹配迭代的最大次数
  ndt->setMaximumIterations(35);
  // 设置要配准的点云
  ndt->setInputSource(Inputpt); //第二次扫描的点云进行体素滤波的结果.
  //设置点云配准目标
  ndt->setInputTarget(Targetptr); //第一次扫描的结果
  //计算需要的刚体变换以便将输入的点云匹配到目标点云
  pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
  ndt->align(*output_cloud, init_guess);
  std::cout << " ndt score: " << ndt->getFitnessScore() << std::endl;
  //使用创建的变换对未过滤的输入点云进行变换
  pcl::transformPointCloud(*Inputpt, *output_cloud, ndt->getFinalTransformation());
  //保存转换的输入点云
  //pcl::io::savePCDFileASCII("./output/ndt_transformed.pcd", *output_cloud);
  // pcl::io::savePLYFileASCII("./output/ndt_transformed.ply", *output_cloud);
  Eigen::Matrix4f transformation = ndt->getFinalTransformation();
  M4ftoM4d(transformation, trans_d);
  return ndt->getFitnessScore();
}
string long2str(long num)
{
  ostringstream oss;
  if (oss << num)
  {
    string str(oss.str());
    return str;
  }
  else
  {
    cout << "[long2str] - Code error" << endl;
    exit(0);
  }
}
string float2str(float num)
{
  ostringstream oss;
  if (oss << num)
  {
    string str(oss.str());
    return str;
  }
  else
  {
    cout << "[float2str] - Code error" << endl;
    exit(0);
  }
}
string int2str(int num)
{
  ostringstream oss;
  if (oss << num)
  {
    string str(oss.str());
    return str;
  }
  else
  {
    cout << "[float2str] - Code error" << endl;
    exit(0);
  }
}
int str2int(string str)
{
  int d;
  stringstream sin(str);
  if (sin >> d)
  {
    return d;
  }
  cout << str << endl;
  cout << "Can not convert a string to int" << endl;
  exit(0);
}
void M4dtoM4f(Eigen::Matrix4d &transformation, Eigen::Matrix4f &trans_d)
{
  trans_d(0, 0) = transformation(0, 0);
  trans_d(0, 1) = transformation(0, 1);
  trans_d(0, 2) = transformation(0, 2);
  trans_d(0, 3) = transformation(0, 3);
  trans_d(1, 0) = transformation(1, 0);
  trans_d(1, 1) = transformation(1, 1);
  trans_d(1, 2) = transformation(1, 2);
  trans_d(1, 3) = transformation(1, 3);
  trans_d(2, 0) = transformation(2, 0);
  trans_d(2, 1) = transformation(2, 1);
  trans_d(2, 2) = transformation(2, 2);
  trans_d(2, 3) = transformation(2, 3);
  trans_d(3, 0) = transformation(3, 0);
  trans_d(3, 1) = transformation(3, 1);
  trans_d(3, 2) = transformation(3, 2);
  trans_d(3, 3) = transformation(3, 3);
}
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d)
{
  trans_d(0, 0) = transformation(0, 0);
  trans_d(0, 1) = transformation(0, 1);
  trans_d(0, 2) = transformation(0, 2);
  trans_d(0, 3) = transformation(0, 3);
  trans_d(1, 0) = transformation(1, 0);
  trans_d(1, 1) = transformation(1, 1);
  trans_d(1, 2) = transformation(1, 2);
  trans_d(1, 3) = transformation(1, 3);
  trans_d(2, 0) = transformation(2, 0);
  trans_d(2, 1) = transformation(2, 1);
  trans_d(2, 2) = transformation(2, 2);
  trans_d(2, 3) = transformation(2, 3);
  trans_d(3, 0) = transformation(3, 0);
  trans_d(3, 1) = transformation(3, 1);
  trans_d(3, 2) = transformation(3, 2);
  trans_d(3, 3) = transformation(3, 3);
}
std::string CreatResultMapPath(int map_id_)
{
    std::string webresultsavedpath=output_pcd_path+int2str(map_id_)+"/";
    const char* DirName0 = webresultsavedpath.data(); 
    int isCreate = mkdir(DirName0, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    webresultsavedpath=webresultsavedpath+"ply/";
    const char* DirName1 = webresultsavedpath.data();
    isCreate = mkdir(DirName1, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if( !isCreate )
        std::cout<<"created path "<<webresultsavedpath<<std::endl;
    else
        std::cout<<"create path failed "<<webresultsavedpath<<std::endl;
    return webresultsavedpath;
}
std::string add0(int x){
    return x<10?("0"+int2str(x)):int2str(x);
}
std::string yd_gettime()
{
    struct timeval tv;
 
    long sec = 0, usec = 0;
    int yy = 0, mm = 0, dd = 0, hh = 0, mi = 0, ss = 0, ms = 0;
    int ad = 0;
    int y400 = 0, y100 = 0, y004 = 0, y001 = 0;
    int m[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int i;
 
    memset(&tv, 0, sizeof(timeval));
 
    gettimeofday(&tv, NULL);
 
    sec = tv.tv_sec;
    usec = tv.tv_usec;
    sec = sec + (60*60)*BEIJINGTIME;
 
    ad = sec/DAY;
    ad = ad - YEARSTART;
    y400 = ad/YEAR400;

    y100 = (ad - y400*YEAR400)/YEAR100;
    y004 = (ad - y400*YEAR400 - y100*YEAR100)/YEAR004;
    y001 = (ad - y400*YEAR400 - y100*YEAR100 - y004*YEAR004)/YEAR001;
 
    yy = y400*4*100 + y100*100 + y004*4 + y001*1 + YEARFIRST;
    dd = (ad - y400*YEAR400 - y100*YEAR100 - y004*YEAR004)%YEAR001;
 
    //月 日
    if(0 == yy%1000)
    {
        if(0 == (yy/1000)%4)
        {
            m[1] = 29;
        }
    }
    else
    {
        if(0 == yy%4)
        {
            m[1] = 29;
        }
    }
 
    for(i = 1; i <= 12; i++)
    {
        if(dd - m[i] < 0)
        {
            break;
        }
        else
        {
            dd = dd -m[i];
        }
    }
 
    mm = i;
    //小时
    hh = sec/(60*60)%24;
    //分
    mi = sec/60 - sec/(60*60)*60;
    //秒
    ss = sec - sec/60*60;
    ms = usec;
    ROS_INFO("%d-%02d-%02d %02d:%02d:%02d.%06d\n", yy, mm, dd, hh, mi, ss, ms);

std::cout<<int2str(yy)+add0(mm)+add0(dd)+add0(hh)+add0(mi)+add0(ss)<<std::endl;
    return int2str(yy)+add0(mm)+add0(dd)+add0(hh)+add0(mi)+add0(ss);
   
}
#endif
