#ifndef LOCALIZE_H
#define LOCALIZE_H
#include "geometry_msgs/PoseStamped.h"
#include <sensor_msgs/PointCloud2.h>
#include "tf_ircampose_msg/start_pose.h"
#include "tf_ircampose_msg/stop_pose.h"
#include "tf_ircampose_msg/tf_pose_status.h"
#include <tf_conversions/tf_eigen.h>
#include <thread>
//synchronism
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
//opencv
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
//pcl
#include <pcl/point_types.h>
#include <pcl/io/io.h>
//lidar
#include "livox_ros_driver/CustomMsg.h"
using namespace cv;
using namespace std;
using namespace pcl;
using namespace message_filters;
class Localization {
public:
    Localization();
private:
    void getparamer();
    void sublivoxCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in);
    bool start_pose(tf_ircampose_msg::start_pose::Request &req,tf_ircampose_msg::start_pose::Response &res);
    bool stop_pose(tf_ircampose_msg::stop_pose::Request &req,tf_ircampose_msg::stop_pose::Response &res);
    void calibpose();
    void CheckTopicTimeOut();
private:
    bool IsLocated=false;//表示是否定位计算完成
    bool IsPubable=false;//表示是否广播定位结果
    bool IsCalibFinished=false;
    ros::ServiceServer star_ircam_pose,stop_ircam_pose;
    std::string calibfilepath;
    std::string motorposetopic,lidartopic;
    std::string datapath,initpcdpath;
    cv::Mat ir_lidar_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat ir_map_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat init_pos_exRT=cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat ndt_pose_exRT=cv::Mat::zeros(4, 4, CV_64F);
    pcl::PointCloud<pcl::PointXYZ> frame, map,neiborinmap;
    ros::Subscriber sub_livox_msg;
    ros::Publisher pub_pose_status;
    int nframe,MaxFrames;
    float H_angle,V_angle;
    int Device_id,Task_id,Map_id;
    std::string plyname,mapFormat;
    ros::Time newpcd_time;
    std::vector<std::string>PLYfilenames;
};
#endif