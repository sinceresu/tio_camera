#ifndef TOOL_H
#define TOOL_H
#include "ros/ros.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/PoseStamped.h"
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include "omp.h"
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
//opencv
#include <Eigen/Core>
#include "eigen3/Eigen/Dense"
#include <opencv2/core.hpp>
#include "opencv2/core/eigen.hpp"
#include <tf/transform_broadcaster.h>
//ros
#include <sensor_msgs/PointCloud2.h>
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/String.h"
#include "geometry_msgs/PoseStamped.h"

//pcl
#include <pcl/point_types.h>
#include <pcl/io/io.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>
#include <pcl/registration/gicp.h>
#include <pcl/registration/ndt.h> //NDT配准类对应头文件
#include "pclomp/ndt_omp.h"
#include <pcl/octree/octree_search.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#define NONE       "\e[0m"
#define L_RED      "\e[1;31m"
#define printlr(format, arg...) do{printf(L_RED format NONE,## arg);}while(0)

using namespace cv;
using namespace std;
using namespace ros;
using namespace pcl;
using namespace sensor_msgs;

typedef Eigen::Matrix<double, 3, 3> Mat33;
typedef Eigen::Matrix<double, 3, 1> Vec3;

void ReadCalibrationFile(std::string _choose_file,cv::Mat &ir_lidar_RT);
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d);
Eigen::Matrix4d ndtprocess(pcl::PointCloud<pcl::PointXYZ>& frame, pcl::PointCloud<pcl::PointXYZ>& map);
void getneighbors(pcl::PointCloud<pcl::PointXYZ> &source, pcl::PointCloud<pcl::PointXYZ> &searchPoint, pcl::PointCloud<pcl::PointXYZ> &neibors);
Eigen::Matrix4d initMatrix(double Rot_H_angel, double Rot_V_angel);
//------------
Eigen::Matrix4d ndtprocess(pcl::PointCloud<pcl::PointXYZ>& frame, pcl::PointCloud<pcl::PointXYZ>& map)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr Inputpt(new pcl::PointCloud<pcl::PointXYZ>(frame));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Targetptr(new pcl::PointCloud<pcl::PointXYZ>(map));
    pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>::Ptr ndt(new pclomp::NormalDistributionsTransform<pcl::PointXYZ, pcl::PointXYZ>());
    ndt->setNeighborhoodSearchMethod(pclomp::DIRECT7);
    ndt->setNumThreads(2);
    ndt->setTransformationEpsilon(0.0001);
    ndt->setStepSize(0.5);
    ndt->setResolution(0.5);
    ndt->setMaximumIterations(10);
    ndt->setInputSource(Inputpt);
    ndt->setInputTarget(Targetptr);
    pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    Eigen::Matrix4f init_guess = Eigen::Matrix4f::Identity();
    ndt->align(*output_cloud, init_guess);
    Eigen::Matrix4f transformation = ndt->getFinalTransformation();
    // pcl::transformPointCloud(*Inputpt, *output_cloud, transformation);
    //pcl::io::savePCDFile("../ndticppc.pcd", *output_cloud);
    Eigen::Matrix4d trans_d;
    M4ftoM4d(transformation, trans_d);
    return trans_d;
}
Eigen::Matrix4d initMatrix(double Rot_H_angel, double Rot_V_angel)
{
    cv::Mat init_exRT = cv::Mat::zeros(4, 4, CV_64F);
    cv::Mat init_R = cv::Mat::zeros(3, 3, CV_64F);
    Eigen::Matrix3d rotation_matrix;
    double rad = 180.0f / M_PI;
    Eigen::Matrix4d transform;
    Eigen::AngleAxisd yawAngle, pitchAngle;
    yawAngle = Eigen::AngleAxisd(Rot_H_angel / rad, Eigen::Vector3d::UnitZ());
    pitchAngle = Eigen::AngleAxisd(Rot_V_angel / rad, Eigen::Vector3d::UnitY());
    rotation_matrix = yawAngle * pitchAngle;
    cv::eigen2cv(rotation_matrix, init_R);
    init_R.copyTo(init_exRT(cv::Rect_<double>(0, 0, 3, 3)));
    init_exRT.at<double>(0, 3) = 0.0;
    init_exRT.at<double>(1, 3) = 0.0;
    init_exRT.at<double>(2, 3) = 0.0;
    init_exRT.at<double>(3, 3) = 1.0;
    cv::cv2eigen(init_exRT, transform);
    return transform;
}
void getneighbors(pcl::PointCloud<pcl::PointXYZ> &source, pcl::PointCloud<pcl::PointXYZ> &searchPoint, pcl::PointCloud<pcl::PointXYZ> &neibors)
{
    float resolution = 0.01f;
    pcl::PointCloud<pcl::PointXYZ>::Ptr Sourceptr(new pcl::PointCloud<pcl::PointXYZ>(source));
    pcl::PointCloud<pcl::PointXYZ>::Ptr Searchptr(new pcl::PointCloud<pcl::PointXYZ>(searchPoint));
    pcl::octree::OctreePointCloudSearch<pcl::PointXYZ> octree(resolution);
    octree.setInputCloud(Sourceptr);
    octree.addPointsFromInputCloud();

    // K nearest neighbor search

    // int K = 10;
    float radius = 0.1f;
    std::vector<int> pointIdxRadiusSearch;
    std::vector<float> pointRadiusSquaredDistance;
    for (pcl::PointCloud<pcl::PointXYZ>::iterator pt = searchPoint.begin();
         pt < searchPoint.end(); pt++)
    {
        if (octree.radiusSearch(*pt, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance) > 0)
        {
            for (std::size_t i = 0; i < pointIdxRadiusSearch.size(); ++i)
                neibors.push_back(source[pointIdxRadiusSearch[i]]);
        }
    }
}

void ReadCalibrationFile(std::string _choose_file,cv::Mat &ir_lidar_RT)
{
    cv::FileStorage fs(_choose_file, cv::FileStorage::READ);
    if (!fs.isOpened())
    {
        std::cout << "Cannot open file calibration file" << _choose_file << std::endl;
    }
    else
    {
        fs["CameraExtrinsicMat"] >> ir_lidar_RT;
    }
}
std::string int2str(int num)
{
    ostringstream oss;
    if (oss << num)
    {
        string str(oss.str());
        return str;
    }
    else
    {
        cout << "[float2str] - Code error" << endl;
        exit(0);
    }
}
void SplitString(const std::string &s, std::vector<std::string> &v, const std::string &c)
{
    std::string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;
    while (std::string::npos != pos2)
    {
        v.push_back(s.substr(pos1, pos2 - pos1));

        pos1 = pos2 + c.size();
        pos2 = s.find(c, pos1);
    }
    if (pos1 != s.length())
        v.push_back(s.substr(pos1));
};
void readFileList(std::string path, std::vector<std::string> &files, std::string ext)
{
    struct dirent *ptr;
    DIR *dir;
    dir = opendir(path.c_str());
    while ((ptr = readdir(dir)) != NULL)
    {
        if (ptr->d_name[0] == '.')
            continue;
        std::vector<std::string> strVec;
        SplitString(ptr->d_name, strVec, ".");

        if (strVec.size() >= 2)
        {
            std::string p;
            if (strVec[strVec.size() - 1].compare(ext) == 0)
                files.push_back(p.assign(path).append("/").append(ptr->d_name));
        }
        else
        {
            std::string p;
            readFileList(p.assign(path).append("/").append(ptr->d_name), files, ext);
        }
    }
    closedir(dir);
};
int YDGetPLYDataFilePath(std::string strFileDirectory,
                         std::vector<std::string> &pcdfiles)
{
    readFileList(strFileDirectory, pcdfiles, "ply");
    return pcdfiles.size();
};
void M4ftoM4d(Eigen::Matrix4f &transformation, Eigen::Matrix4d &trans_d)
{
    trans_d(0, 0) = transformation(0, 0);
    trans_d(0, 1) = transformation(0, 1);
    trans_d(0, 2) = transformation(0, 2);
    trans_d(0, 3) = transformation(0, 3);
    trans_d(1, 0) = transformation(1, 0);
    trans_d(1, 1) = transformation(1, 1);
    trans_d(1, 2) = transformation(1, 2);
    trans_d(1, 3) = transformation(1, 3);
    trans_d(2, 0) = transformation(2, 0);
    trans_d(2, 1) = transformation(2, 1);
    trans_d(2, 2) = transformation(2, 2);
    trans_d(2, 3) = transformation(2, 3);
    trans_d(3, 0) = transformation(3, 0);
    trans_d(3, 1) = transformation(3, 1);
    trans_d(3, 2) = transformation(3, 2);
    trans_d(3, 3) = transformation(3, 3);
}
void SaveCalibrationFile(std::string path_filename_str,cv::Mat in_extrinsic)
{

    std::cout<<"the yaml has wrote in: "<<path_filename_str<<std::endl;
    cv::FileStorage fs(path_filename_str.c_str(), cv::FileStorage::WRITE);
    if (!fs.isOpened())
    {
        fprintf(stderr, "%s : cannot open file\n", path_filename_str.c_str());
        exit(EXIT_FAILURE);
    }

    fs << "CameraExtrinsicMat" << in_extrinsic;

}
#endif
