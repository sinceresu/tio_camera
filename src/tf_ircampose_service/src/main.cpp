#include "ros/ros.h"
#include "localize.h"

int main(int argc, char **argv)
{
    ros::init(argc, argv, "tf_ircam_pose");
    Localization locator;
    ros::Rate loop_rate(10);
    while(ros::ok()){
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}