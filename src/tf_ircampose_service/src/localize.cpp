#include "tool.h"
#include "localize.h"
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
Localization::Localization()
{
    ros::NodeHandle n;
    ROS_INFO("start tf_ircam_pose");
    newpcd_time = ros::Time::now();
    getparamer();
    map.resize(0);
    PLYfilenames.resize(0);
    ReadCalibrationFile(calibfilepath,ir_lidar_exRT);
    star_ircam_pose = n.advertiseService("/tio/inspect/ircam_pose_start",  &Localization::start_pose, this);//开始定位
    stop_ircam_pose = n.advertiseService("/tio/inspect/ircam_pose_stop",  &Localization::stop_pose, this);//停止发送定位
    sub_livox_msg = n.subscribe("/livox/lidar", 1, &Localization::sublivoxCbk, this);
    pub_pose_status=n.advertise<tf_ircampose_msg::tf_pose_status>("/tio/irpose/pose_status", 1);
    std::thread calibthd(std::bind(&Localization::calibpose, this));
    calibthd.detach();
    std::thread checkthd(std::bind(&Localization::CheckTopicTimeOut, this));
    checkthd.detach();
}
void Localization::CheckTopicTimeOut()
{
    while (1) {
        sleep(0.1);
        if (IsPubable&&(ros::Time::now()-newpcd_time>ros::Duration(60.0f)||newpcd_time-ros::Time::now()>ros::Duration(60.0f)))
        {
            tf_ircampose_msg::tf_pose_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="Unable to get pcd from livox lidar.";
            pub_pose_status.publish(web_task_msg);
        }

    }
}
bool Localization::start_pose(tf_ircampose_msg::start_pose::Request &req,tf_ircampose_msg::start_pose::Response &res)
{//开始采集数据，读取当前角度，采集足够帧数后，计算定位
    ROS_INFO("start_ircampose recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl
            <<"V_angle is "<<req.pitch_angle<<std::endl
            <<"H_angle is "<<req.roll_angle<<std::endl
            <<"stationMapVersion is: "<<req.map_version<<std::endl
            <<"mapFormat is: "<<req.map_format<<std::endl;
    if(map.size()==0||Map_id!=req.map_id||strcmp(plyname.c_str(),(req.map_version).c_str())!=0||strcmp(mapFormat.c_str(),(req.map_format).c_str())!=0)
        //first localize,no map date or new map task
    {
        Device_id=req.device_id;
        Task_id=req.task_id;
        Map_id=req.map_id;
        plyname=req.map_version;
        mapFormat=req.map_format;
        std::string mappath=datapath+"/"+int2str(Map_id)+"/ply/"+plyname+"."+mapFormat;
        std::cout<<"need to load map: "<<mappath<<std::endl;
        if (access(mappath.c_str(), 0) == -1)
        {
            std::cout<<"The filepath "<<mappath<<" is not exist."<<std::endl;
            tf_ircampose_msg::tf_pose_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="The filepath is not exist.";
            pub_pose_status.publish(web_task_msg);
            res.success =false;
            res.message="The filepath is not exist.";
            return false;
        }
        // PLYfilenames.resize(0);
        // if(YDGetPLYDataFilePath(mappath, PLYfilenames)!=1)//如果map id 文件夹下的ply不唯一则报错
        // {
        //     std::cout<<"The ply file under the map_id file is not only one."<<std::endl;
        //     tf_ircampose_msg::tf_pose_status web_task_msg;
        //     web_task_msg.device_id=Device_id;
        //     web_task_msg.task_id=Task_id;
        //     web_task_msg.map_id=Map_id;
        //     web_task_msg.task_status=-1;//异常
        //     web_task_msg.error_message="The ply file under the map_id file is not only one.";
        //     pub_pose_status.publish(web_task_msg);
        //     res.success =false;
        //     res.message="The ply file under the map_id file is not only one.";
        //     return false;

        // }
        // std::cout<<"Get map from: "<<PLYfilenames[0]<<std::endl;
        if (pcl::io::loadPLYFile(mappath, map) == -1)
        {
            PCL_ERROR("Couldn't read map files\n");
            tf_ircampose_msg::tf_pose_status web_task_msg;
            web_task_msg.device_id=Device_id;
            web_task_msg.task_id=Task_id;
            web_task_msg.map_id=Map_id;
            web_task_msg.task_status=-1;//异常
            web_task_msg.error_message="Couldn't read map files.";
            pub_pose_status.publish(web_task_msg);
            res.success =false;
            res.message="Couldn't read map files.";
            return false;
        }

    }
    H_angle=req.roll_angle;
    V_angle=req.pitch_angle;
    IsLocated=false;
    IsPubable=true;
    nframe=0;
    frame.resize(0);
    res.success =true;
    res.message="start_ircampose";
    return true;
}
bool Localization::stop_pose(tf_ircampose_msg::stop_pose::Request &req,tf_ircampose_msg::stop_pose::Response &res)
{//停止发送tf
    ROS_INFO("stop_ircampose recieve request");
    std::cout
            <<"device_id is "<<req.device_id<<std::endl
            <<"task_id is "<<req.task_id<<std::endl
            <<"map_id is "<<req.map_id<<std::endl;
    Device_id=req.device_id;
    Task_id=req.task_id;
    Map_id=req.map_id;
    IsLocated=false;
    IsPubable=false;
    IsCalibFinished=false;
    nframe=0;
    frame.resize(0);
    res.success =true;
    res.message="stop_ircampose";
    return true;
}
void Localization::sublivoxCbk(const sensor_msgs::PointCloud2ConstPtr &livox_msg_in)
{
    newpcd_time = ros::Time::now();
    pcl::PointCloud<pcl::PointXYZ> laserCloudIn;
    pcl::fromROSMsg(*livox_msg_in, laserCloudIn);
    //可发送tf，但帧数不够01
    if(nframe<MaxFrames&&IsPubable)
    {
        frame.insert(frame.end(), laserCloudIn.begin(), laserCloudIn.end());
        nframe++;
    }
    // //可发送tf，并已定位计算11
    // else if(IsLocated&&IsPubable)
    // {
    //     Eigen::Matrix3d rotation_matrix;
    //     cv::Mat handin_R = ir_map_exRT(cv::Rect(0, 0, 3, 3));
    //     cv::cv2eigen(handin_R, rotation_matrix);
    //     static tf::TransformBroadcaster br;//定义一个广播
    //     tf::Quaternion quad;
    //     tf::Transform trans;
    //     Vec3 eulerAngle = rotation_matrix.eulerAngles(0,1,2);
    //     trans.setOrigin(tf::Vector3(ir_map_exRT.at<double>(0, 3), ir_map_exRT.at<double>(1, 3), ir_map_exRT.at<double>(2, 3)));
    //     quad.setRPY(eulerAngle[0],eulerAngle[1],eulerAngle[2]);
    //     trans.setRotation(quad);
    //     br.sendTransform(tf::StampedTransform(trans, ros::Time::now(),"tio_ircam_in_world","ircam_pose"));
    //     frame.resize(0);
    // }
}
void Localization::calibpose()
{
    while (1) {
        sleep(0.1);

        if(!IsLocated&&IsPubable&&nframe==MaxFrames)
        {
            IsLocated = true;//定位完成，可以发送tf
            double dur;
            clock_t start, end;
            start = clock();
            std::cout<<"+++calibing pose+++"<<std::endl;
            Eigen::Matrix4d transform = initMatrix(H_angle,V_angle);
            cv::eigen2cv(transform, init_pos_exRT);
            pcl::PointCloud<pcl::PointXYZ>::Ptr init_cloud(new pcl::PointCloud<pcl::PointXYZ>());
            pcl::transformPointCloud(frame, *init_cloud, transform); //粗配位置
            pcl::io::savePCDFile(initpcdpath+"init.pcd", *init_cloud);
            frame.resize(0);
            try {
                getneighbors(map, *init_cloud, neiborinmap);
                std::cout<<"getneighbors finished"<<std::endl;
                transform = ndtprocess(*init_cloud, neiborinmap);//ndt细匹配
                pcl::PointCloud<pcl::PointXYZ>::Ptr ndt_cloud(new pcl::PointCloud<pcl::PointXYZ>());
                pcl::transformPointCloud(*init_cloud, *ndt_cloud, transform);
                pcl::io::savePCDFile(initpcdpath+"ndted.pcd", *ndt_cloud);
                neiborinmap.resize(0);
                std::cout<<"ndtprocess finished"<<std::endl;
                cv::eigen2cv(transform, ndt_pose_exRT);
                ir_lidar_exRT.convertTo(ir_lidar_exRT, CV_64F);
                init_pos_exRT.convertTo(init_pos_exRT, CV_64F);
                ndt_pose_exRT.convertTo(ndt_pose_exRT, CV_64F);
                Eigen::Isometry3d RT1_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT2_trans = Eigen::Isometry3d::Identity();
                Eigen::Isometry3d RT3_trans = Eigen::Isometry3d::Identity();
                cv::cv2eigen(ir_lidar_exRT, RT1_trans.matrix());
                cv::cv2eigen(init_pos_exRT, RT2_trans.matrix());
                cv::cv2eigen(ndt_pose_exRT, RT3_trans.matrix());
                Eigen::Isometry3d delta = RT3_trans * RT2_trans * RT1_trans;
                cv::eigen2cv(delta.matrix(), ir_map_exRT);
                std::cout << "ir->map exRT:" << std::endl
                          << ir_map_exRT << std::endl;
                //test 归一化方法不对
                //Eigen::Matrix3d rotation;
                //cv::Mat in_R = ir_map_exRT(cv::Rect(0, 0, 3, 3));
                //cv::cv2eigen(in_R, rotation);
                //Eigen::Vector3d eulerAngle = rotation.eulerAngles(2, 1, 0);
                //Eigen::AngleAxisd rollAngle(Eigen::AngleAxisd(eulerAngle(2), Eigen::Vector3d::UnitX()));
                //Eigen::AngleAxisd pitchAngle(Eigen::AngleAxisd(eulerAngle(1), Eigen::Vector3d::UnitY()));
                //Eigen::AngleAxisd yawAngle(Eigen::AngleAxisd(eulerAngle(0), Eigen::Vector3d::UnitZ())); 
                //rotation = yawAngle * pitchAngle * rollAngle;                                       
               //cv::eigen2cv(rotation, in_R);
                //in_R.copyTo(ir_map_exRT(cv::Rect_<double>(0, 0, 3, 3)));
                //
                //std::cout << "after ir->map exRT:" << std::endl
                         // << ir_map_exRT << std::endl;
                SaveCalibrationFile(initpcdpath+"temp.yaml",ir_map_exRT);
                end = clock();
                dur = (double) (end - start);
                printf("using Time:%f ms\n ", (dur / CLOCKS_PER_SEC));
                IsCalibFinished=true;
            } 
            catch(std::exception ex)
            {
                printlr("Error: localize faile, recollect current sector and retrying......\n");
//                PCL_ERROR("Build map failed in %s \n",filenumstr);
//                std_msgs::Int32 msg;
//                msg.data = 0;
                nframe = 0;
//                pub_nframes.publish(msg);
            }
        }
        if(IsCalibFinished&&IsLocated&&IsPubable)
        {
            //tf2
            static tf2_ros::TransformBroadcaster br;
            geometry_msgs::TransformStamped transformStamped;
            transformStamped.header.stamp = ros::Time::now();
            transformStamped.header.frame_id = "tio_ircam_in_world";
            transformStamped.child_frame_id = "ircam_pose";
            transformStamped.transform.translation.x = ir_map_exRT.at<double>(0, 3);
            transformStamped.transform.translation.y = ir_map_exRT.at<double>(1, 3);
            transformStamped.transform.translation.z = ir_map_exRT.at<double>(2, 3);
            Eigen::Isometry3d RT_trans = Eigen::Isometry3d::Identity();
            cv::cv2eigen(ir_map_exRT, RT_trans.matrix());
            Eigen::Quaterniond rotation(RT_trans.linear());
            Eigen::Vector3d position=RT_trans.translation();
            tf2::Quaternion q(rotation.x(),rotation.y(),rotation.z(),rotation.w());
            transformStamped.transform.rotation.x = q.x();
            transformStamped.transform.rotation.y = q.y();
            transformStamped.transform.rotation.z = q.z();
            transformStamped.transform.rotation.w = q.w();
            br.sendTransform(transformStamped);
            frame.resize(0);
        }

    }

}
void Localization::getparamer()
{

    cout << "Get the parameters from the launch file" << endl;
    if (!ros::param::get("calibfilepath", calibfilepath))
    {
        cout << "Can not get the value of calibfilepath" << endl;
        exit(1);
    }
    if (!ros::param::get("motorposetopic", motorposetopic))
    {
        cout << "Can not get the value of motorposetopic" << endl;
        exit(1);
    }
    if (!ros::param::get("MaxFrames", MaxFrames))
    {
        cout << "Can not get the value of MaxFrames" << endl;
        exit(1);
    }
    if (!ros::param::get("lidartopic", lidartopic))
    {
        cout << "Can not get the value of lidartopic" << endl;
        exit(1);
    }
    if (!ros::param::get("mappath", datapath))
    {
        cout << "Can not get the value of mappath" << endl;
        exit(1);
    }
    if (!ros::param::get("initpcdpath", initpcdpath))
    {
        cout << "Can not get the value of initpcdpath" << endl;
        exit(1);
    }

//    if (pcl::io::loadPCDFile<pcl::PointXYZ>(mappath, map) == -1)
//    {
//        PCL_ERROR("Couldn't read mapfile\n");
//        exit(1);
//    }

}
