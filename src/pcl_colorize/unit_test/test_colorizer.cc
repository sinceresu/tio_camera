#include <algorithm>
#include <fstream>
#include <iostream>

#include <opencv2/opencv.hpp>
#include <Eigen/Geometry>
#include <pcl/io/pcd_io.h>


#include "gflags/gflags.h"
#include "glog/logging.h"

#include "pcl_colorize_interface.h"
#include "libpcl_colorize.h"

DEFINE_string(
    calibration_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");              

DEFINE_string(
    image_filepath, "",
"Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");

DEFINE_string(
    pcl_filepath, "",
    "Comma-separated list of bags to process. One bag per trajectory. "
    "Any combination of simultaneous and sequential bags is supported.");


using namespace std;
namespace pcl_colorize{

namespace {

}

void Test(int argc, char** argv) {
    auto pcl_colorizer = CreatePclColorizer();
    pcl_colorizer->SetCalibrationFile(FLAGS_calibration_filepath);
    pcl::PointCloud<pcl::PointXYZ>::Ptr raw_map(new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::io::loadPCDFile<pcl::PointXYZ>(FLAGS_pcl_filepath, *raw_map);

    pcl_colorizer->SetRawPointCloud(raw_map);

    cv::Mat image = cv::imread(FLAGS_image_filepath);
    Eigen::Affine3f pose = Eigen::Affine3f::Identity();
    cv::Mat colored_image;

    pcl_colorizer->Colorize(image, pose, colored_image);

}

}


int main(int argc, char** argv) {
  google::InitGoogleLogging(argv[0]);
  google::ParseCommandLineFlags(&argc, &argv, true);
  FLAGS_logtostderr = true;


  pcl_colorize::Test(argc, argv);

}
