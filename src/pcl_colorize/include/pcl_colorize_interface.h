#ifndef _PCL_COLORIZE_PCL_COLORIZE_INTERFACE_H
#define _PCL_COLORIZE_PCL_COLORIZE_INTERFACE_H

#include <vector>
#include <string>
#include <memory>
#include <functional>
#include <stdint.h>

#include <opencv2/opencv.hpp>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>


namespace pcl_colorize{
class PclColorizeInterface
{
public:
  virtual ~PclColorizeInterface(){};


    /******************************************************************************
  * \fn PclColorizeInterface.SetCameraParameter
  * Create Time: 2020/06/02
  * Description: -
  *    设置Camera参数
  *
  * \param calibration_filepath 输入参数
  * 		
  *
  * \return
  * 		错误码
  *
  * \note 
  *******************************************************************************/
  virtual  int SetCalibrationFile(const std::string & calibration_filepath) = 0 ;

  /******************************************************************************
  * \fn PclColorizeInterface.SetRawPointCloud
  * Create Time: 2020/06/02
  * Description: -
  *   设置点云文件
  *
  * \param pointcloud_filepath 输入参数
   *  点云文件
  * 
  * \return
  * 		错误码
  *
  * \note 。
  *******************************************************************************/
  virtual  int  SetRawPointCloud(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr & pointcloud) = 0 ;
  /******************************************************************************
  * \fn PclColorizeInterface.Colorize
  * Create Time: 2020/06/02
  * Description: -
  *   设置点云文件
  *
  * \param image 输入参数
  *  温度图片
  * 
  *
  * \param cam_pose 输入参数
  *  位姿
  * \param temp_img_3d 输出参数
  *  温度＋三维坐标图片
  * 
  * \return
  * 		错误码
  *
  * \note 。
  *******************************************************************************/
  virtual int Colorize(const cv::Mat& image, const Eigen::Affine3f &cam_pose, cv::Mat& temp_img_3d) = 0 ;
  virtual int GetCameraFov(double &horizontal_fov, double& vertical_fov) = 0;
  // virtual  int  ColorizeVideoFrames(std::vector<std::vector<std::string>>&image_filenames, const std::vector<Eigen::Matrix4f> & track_poses, , const std::vector<double> &  offset_secs, const std::string & save_directory,  const std::string& map_name) = 0 ;

};
} // namespace pcl_colorize

#endif  
