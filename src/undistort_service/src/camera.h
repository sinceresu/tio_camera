#ifndef _UNDISTORT_SERVICE_CAMERA_H
#define _UNDISTORT_SERVICE_CAMERA_H

#include <vector>
#include <memory>
#include <thread>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "opencv2/opencv.hpp"

namespace undistort_service{

class Camera 
{
public:
  Camera();
  ~Camera(){};

   int LoadCalibrationFile(const std::string& calibration_filepath);
   int Undistort(const cv::Mat& image,  cv::Mat& undistort_image);
   Eigen::Matrix3d GetIntrinsic() ;

private:
  // expressed in the camera frame.
  cv::Mat intrinsic_mat_;  
  cv::Mat centered_intrinsic_mat_;  
  cv::Mat distortion_coeffs_;
  cv::Size image_size_;
  cv::Mat map_x_, map_y_;


};
} // namespace undistort_service

#endif  
