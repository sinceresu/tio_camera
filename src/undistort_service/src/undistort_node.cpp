#include "undistort_node.h"
#include <limits>
#include <iostream>
#include <fstream>
#include <thread> 
#include <boost/filesystem.hpp>

#include "ros/package.h"
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Image.h>
#include "tf2_eigen/tf2_eigen.h"

#include "gflags/gflags.h"
#include "glog/logging.h"

#include "undistort_service/node_constants.h"

#include "camera.h"


using namespace std;
using namespace cv;

namespace undistort_service {
constexpr double kTfBufferCacheTimeInSeconds = 10.;

namespace {
constexpr int kLatestOnlyPublisherQueueSize = 1;

// Subscribes to the 'topic' for 'trajectory_id' using the 'node_handle' and
// calls 'handler' on the 'node' to handle messages. Returns the subscriber.
template <typename MessageType>
::ros::Subscriber SubscribeWithHandler(
    void (UndistortNode::*handler)(const std::string&,
                                    const typename MessageType::ConstPtr&),
      const std::string& topic,
    ::ros::NodeHandle* const node_handle, UndistortNode* const node) {
  return node_handle->subscribe<MessageType>(
      topic, kLatestOnlyPublisherQueueSize,
      boost::function<void(const typename MessageType::ConstPtr&)>(
          [node, handler,
           topic](const typename MessageType::ConstPtr& msg) {
            (node->*handler)(topic, msg);
          }));
}
}

UndistortNode::UndistortNode() :
  buffer_(),
  listener_(buffer_),
  state_(UndistortState::IDLE),
  calib_file_updated_(false)
 {

  GetParameters();

  camera_ = make_shared<Camera>();
  camera_->LoadCalibrationFile(calibration_filepath_);

  undistort_result_publisher_= node_handle_.advertise<undistort_service_msgs::PosedImage>(
        undistort_topic_, 1);    

  service_servers_.push_back(node_handle_.advertiseService(
      kStartUndistortServiceName, &UndistortNode::HandleStartUndistort, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kStopUndistortServiceName, &UndistortNode::HandleStopUndistort, this));

  service_servers_.push_back(node_handle_.advertiseService(
      kUpdateCalibParamsServiceName, &UndistortNode::HandleUpdateCalibParams, this));

}

UndistortNode::~UndistortNode() {
  if (temp_image_subscriber_)  {
    temp_image_subscriber_.shutdown();
  }

 }

void UndistortNode::GetParameters() {
  
  ::ros::NodeHandle private_handle("~");

  private_handle.param<std::string>("/undistort_service/raw_topic", raw_topic_, "/fixed/infrared/raw");

  private_handle.param<std::string>("/undistort_service/map_frame", map_frame_, "map");
  private_handle.param<std::string>("/undistort_service/robot_frame", robot_frame_, "lidar_pose");

  private_handle.param<std::string>("/undistort_service/undistort_topic", undistort_topic_, "/infrared/undistort");


  ros::param::get("~calib_filepath", calibration_filepath_);

}



bool UndistortNode::HandleStartUndistort(
        undistort_service_msgs::start_undistort::Request& request,
        undistort_service_msgs::start_undistort::Response& response) {
  if (state_ == UndistortState::UNDISTORTING) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "undistorting.";
      LOG(WARNING) << "undistorting! " ;
      return true;  
  }

  temp_image_subscriber_ = SubscribeWithHandler<sensor_msgs::Image>(
            &UndistortNode::HandleTempImage, raw_topic_, &node_handle_,
            this);

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::UNDISTORTING;
  return true;
}

bool UndistortNode::HandleStopUndistort(
        undistort_service_msgs::stop_undistort::Request& request,
        undistort_service_msgs::stop_undistort::Response& response) {
  if (state_ == UndistortState::IDLE) {
      response.status.code = UndistortResult::SUCCESS;
      response.status.message = "not undistorting.";
      LOG(WARNING) << "not undistorting! " ;
      return true;  
  }


  temp_image_subscriber_.shutdown();
  std::this_thread::sleep_for(std::chrono::milliseconds(50));

  response.status.code = UndistortResult::SUCCESS;
  state_ = UndistortState::IDLE;
  return true;
}
      


bool UndistortNode::HandleUpdateCalibParams(
        undistort_service_msgs::update_calib_params::Request& request,
        undistort_service_msgs::update_calib_params::Response& response) {
  LOG(INFO) << "Enter HandleUpdateCalibParamsService . " ;

  ofstream calib_file(calibration_filepath_);
  calib_file << request.calib_params[0];

  calib_file_updated_ = true;

  response.status.code = UndistortResult::SUCCESS;

  LOG(INFO) << "Exit HandleUpdateCalibParamsService . " ;
  return true;
}


void UndistortNode::HandleTempImage( const std::string& topic, const sensor_msgs::Image::ConstPtr& img_msg) {
  // ROS_INFO("HandleTempImage");
  if (calib_file_updated_) {
    camera_->LoadCalibrationFile(calibration_filepath_);
    calib_file_updated_ = false;
  }
  geometry_msgs::TransformStamped robot_to_map_transform;
  try{
  robot_to_map_transform = buffer_.lookupTransform(
          map_frame_, robot_frame_, img_msg->header.stamp, ::ros::Duration(kTfBufferCacheTimeInSeconds));
  } catch (tf2::TransformException &ex) {
    LOG(WARNING) <<  "Could NOT transform " << robot_frame_ << " to " << map_frame_<< " : " <<  ex.what();
    return;
  }

  LOG_EVERY_N(INFO, 100) << "lookupTransform " << topic << " from " << robot_frame_ << " to " << map_frame_<< "  ok. ";
  
  Eigen::Affine3d robot_to_map =  tf2::transformToEigen (robot_to_map_transform);

  cv_bridge::CvImageConstPtr cv_ptr= cv_bridge::toCvCopy(*img_msg, sensor_msgs::image_encodings::TYPE_32FC1);
	// cv::Mat undistort_image;
  // camera_->Undistort(cv_ptr->image, undistort_image);

  cv_bridge::CvImage heat_cvimg(img_msg->header,
                            sensor_msgs::image_encodings::TYPE_32FC1, cv_ptr->image);

  auto image_msg = heat_cvimg.toImageMsg();
  undistort_service_msgs::PosedImage PosedImage;
  PosedImage.image = *image_msg;
  PosedImage.image.header =  img_msg->header;

  PosedImage.pose = Eigen::toMsg (robot_to_map);

  // sensor_msgs::CameraInfo camera_info;
  // Eigen::Matrix3d camera_intrinsic = camera_->GetIntrinsic();
  // for (int i = 0; i < camera_intrinsic.rows(); ++i) {
  //   for (int j = 0; j < camera_intrinsic.cols(); ++j) {
  //     camera_info.K[i * camera_intrinsic.cols() + j] = camera_intrinsic(i, j);
  //   }
  // }
  // PosedImage.camera_info = camera_info;
  // LOG(INFO) << "undistort_result_publisher_.publish(PosedImage)";
  undistort_result_publisher_.publish(PosedImage);

}



}

