#include "camera.h"
#include <thread>
#include <chrono>
#include <algorithm>
#include <math.h>

#include <Eigen/Core>
#include <opencv2/opencv.hpp>
#include<opencv2/core/eigen.hpp>
#include "glog/logging.h"

#include "common/err_code.h"

using namespace std;

  
namespace undistort_service{
namespace {

}

Camera::Camera() 
{

} 

int Camera::Undistort(const cv::Mat& image,  cv::Mat& undistort_image)  {
  
  CHECK(!intrinsic_mat_.empty() && !distortion_coeffs_.empty()) << "set parameters first!";
  // cv::fisheye::undistortImage(image, undistorted_img, intrinsic_mat_, distortion_coeffs_, new_camera_instrinsics_);
  cv::remap(image, undistort_image, map_x_, map_y_,  cv::INTER_LINEAR);

  return ERRCODE_OK;
}

int Camera::LoadCalibrationFile(const std::string& calibration_filepath) {
	cv::FileStorage fs;

  fs.open(calibration_filepath, cv::FileStorage::READ);
  if(!fs.isOpened()){
      LOG(FATAL) << "can't open calibration file " << calibration_filepath <<  "." ;
      return ERRCODE_FAILED;
  }
    
  fs["CameraMat"] >> intrinsic_mat_;
  fs["DistCoeff"] >> distortion_coeffs_;
  fs["ImageSize"] >> image_size_;

  cv::Mat cam_extrinsic_mat;

  fs.release();   

  centered_intrinsic_mat_ =cv::getDefaultNewCameraMatrix ( intrinsic_mat_, image_size_, true);
  cv::initUndistortRectifyMap(intrinsic_mat_, distortion_coeffs_, cv::Mat(), centered_intrinsic_mat_, image_size_, CV_32FC1, map_x_, map_y_);

  return ERRCODE_OK;

 }

Eigen::Matrix3d Camera::GetIntrinsic() {
  Eigen::Matrix3d intrinsic_eigen;
  cv::cv2eigen(centered_intrinsic_mat_, intrinsic_eigen);
  return intrinsic_eigen;

}

}
