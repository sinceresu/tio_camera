**建图**
#start_mapping.srv#
用于启动开始建图
Request/Response:
请求中，int16 resolution_flag可配置为0/1/2，分标表示了低中高三档密度，影响雷达采集数据的时长，
接收到该请求时，如果可正常启动开始建图，bool success为true，异常时bool success为false，string message中包含错误信息；
rosmessage:
启动开始建图后
会发送一个type为tio_msg::mapping_status、topic为/tio/map/mapping_status的消息，
其中，tio_msg::mapping_status类型中的task_status=0

#stop_mapping.srv#
Request/Response:
用于响应“结束采集流程，可以开始全图着色”的操作，收到该请求后，表示不会再接收新扇区数据，
接收到该请求时进行回馈，正常时bool success为true，异常时bool success为false，string message中包含错误信息；
rosmessage:
结束采集流程
会发送一个type为tio_msg::mapping_status、topic为/tio/map/mapping_status的消息，
其中，tio_msg::mapping_status类型中的task_status=3

#quit_mapping.srv#
Request/Response:
用于响应“终止当前建图”的操作，收到该请求后，会在完成当前扇区采集任务完成后，清空以采集的全部数据，
接收到该请求时进行回馈，正常时bool success为true，异常时bool success为false，string message中包含错误信息；
rosmessage:
终止建图后
会发送一个type为tio_msg::mapping_status、topic为/tio/map/mapping_status的消息，
其中，tio_msg::mapping_status类型中的task_status=2

#collect_data.srv#
用于电机旋转到位后，开始采集当前扇区的雷达数据和彩色图像数据
Request/Response:
请求中，float32 pitch_angle为电机旋转到的垂直角度，float32 roll_angle为电机旋转到的水平角度，
接收到该请求时进行回馈，正常时bool success为true，异常时bool success为false，string message中包含错误信息；
采集过程中，如果60秒超时未收到图像数据或雷达数据，会发送一个type为tio_msg::mapping_status、topic为/tio/map/mapping_status的消息，
其中，tio_msg::mapping_status类型中的task_status=-1，error_message="Unable to get image from color camera."或error_message="Unable to get pcd from livox lidar."
rosmessage:
当该扇区内的数据采集并拼接完该扇区后，会发送一个type为tio_msg::mapping_progres、stopic为/tio/collected_frame_cnt的消息，用于判断当前扇区是否采集完毕；
data等于0表示该扇区拼接失败，需要重新发送采集该区数据的请求

**可见光全地图着色**
rosmessage:
在采集完足够的扇区数据并建图完成后会进行可见光全地图着色，会发送一个type为tio_msg::mapping_progres、topic为/tio/map/mapping_progress的消息，用于记录建图进度0.0~1.0
当可见光全地图着色完成后即整个流程完成后，会发送一个type为tio_msg::mapping_status、topic为/tio/map/mapping_status的消息，
其中，tio_msg::mapping_status类型中的task_status=1

**消息**
#mapping_progress.msg#
其中的float32 progress表示建图进程，边采集边建图占50%，可见光全地图着色占50%
#mapping_status.msg#
其中的uint8 task_status表示建图状态，
0 已开始
1 已正常结束
2 已强制终止
3 结束采集整体流程
-1 异常，异常原因见error_message
