# fixed_yd_motor

作为一个tcp client发送控制指令给电机

## Compile
cd ~/workspace && catkin_make

## Start Run

roslaunch fixed_yd_motor motor2.launch  


## Params
- motor2.launch  
```json
<launch>
  <node pkg="fixed_yd_motor" type="ptz_node2" name="ptz_node2" respawn="false" output="screen">
      <param name="camera_id" type="int" value="1"/>
      <param name="device_ip" value="192.168.1.201"/>
      <param name="device_port" type="int" value="3000"/>
</node>
</launch>
```